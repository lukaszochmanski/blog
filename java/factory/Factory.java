package com.factory;

import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.stream.Stream;

public interface Factory<T> {

    Class<Factory> FACTORY_CLASS = Factory.class;

    static ParameterizedType getFactoryInterface(Class<? extends Factory> factory) {
        return (ParameterizedType) Stream.of(factory.getGenericInterfaces())
                .filter(Factory::isEqualToFactoryClass)
                .findAny()
                .orElseThrow(() -> noFactoryFoundException(factory));
    }

    static boolean isEqualToFactoryClass(Type p) {
        return FACTORY_CLASS == ((ParameterizedType) p).getRawType();
    }

    static RuntimeException noFactoryFoundException(Class<? extends Factory> factory) {
        return new RuntimeException("Could not find \"class " + factory + " that implements " + FACTORY_CLASS + "\"");
    }

    default T create() {
        assertIsNotAProxy();
        assertIsNotAService();
        try {
            return null == getObjectClass().getDeclaringClass() ? getObjectClass().newInstance() : createObjectOfInnerOrNestedClass();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    default T createObjectOfInnerOrNestedClass() throws InstantiationException, IllegalAccessException {
        try {
            return getNestedOrInner(getObjectClass());
        } catch (NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    default T getNestedOrInner(Class<?> cls) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return Modifier.isStatic(cls.getModifiers()) ? getNestedClass(cls) : getInnerClass(cls.getDeclaringClass().newInstance(), cls);
    }

    @SuppressWarnings("unchecked")
    default T getInnerClass(Object parent, Class<?> cls) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return (T) cls.getDeclaredConstructor(new Class[]{parent.getClass()}).newInstance(parent);
    }

    @SuppressWarnings("unchecked")
    default T getNestedClass(Class<?> cls) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return (T) cls.getDeclaredConstructor(new Class[]{}).newInstance();
    }

    @SuppressWarnings("unchecked")
    default Class<T> getObjectClass() {
        return (Class) getObjectType();
    }

    default Type getObjectType() {
        return getFactoryInterface().getActualTypeArguments()[0];
    }

    default ParameterizedType getFactoryInterface() {
        final ParameterizedType factoryInterface = getFactoryInterface(getClass());
        assertThatExactlyOneArgumentIsGiven(factoryInterface);
        return factoryInterface;
    }

    default void assertThatExactlyOneArgumentIsGiven(ParameterizedType factoryInterface) {
        if (null == factoryInterface
                || null == factoryInterface.getActualTypeArguments()
                || factoryInterface.getActualTypeArguments().length != 1) {
            throw new RuntimeException("You can only use generic version of " + FACTORY_CLASS + " with one type argument.");
        }
    }

    default boolean isSingleton() {
        return false;
    }

    default void assertIsNotAService() {
        if (getObjectClass().isAnnotationPresent(Service.class)) {
            throw new RuntimeException("You must use Dependency Injection not a constructor for " + getObjectClass());
        }
    }

    default void assertIsNotAProxy() {
        if (Stream.of(getClass().getGenericInterfaces()).anyMatch(p -> p.getTypeName().contains("proxy"))) {
            throw new RuntimeException("Unfortunatelly our generic factory doesn't work well with proxies, "
                    + "so you need to override create() or override assertIsNotAProxy() to get rid of this exception.");
        }
    }
}
