import com.basf.gpdoa.base.model.entity.Nameable;
import com.basf.gpdoa.base.util.DateUtils;
import java.util.Date;

public interface ExcelExportColumn<T extends ExcelExportable<T>> {

    String YES = "Ja";
    String NO = "Nein";

    int getColumnIndex();

    String getValue(final T object);

    int getColumnWidth();

    String getHeadline();

    static String fromBoolean(final Boolean value) {
        return value ? YES : NO;
    }

    static <S extends Nameable<S>> String toStringg(final Nameable<S> value) {
        return value.getName();
    }

    static String toStringg(final Date value) {
        return DateUtils.toGermanDateString(value);
    }
}
