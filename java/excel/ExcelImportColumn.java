import com.basf.gpdoa.base.exception.GPDOAServiceException;
import com.basf.gpdoa.base.util.DateUtils;
import com.basf.gpdoa.zao.model.entity.Salutation;
import com.google.common.collect.Maps;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.Cell.*;
import org.apache.poi.ss.usermodel.DataFormatter;

public interface ExcelImportColumn<T extends ExcelImportable<T>> {

    char getColumnSymbol();

    int getColumnIndex();

    String getColumnName();

    BiFunction<ExcelImportColumn<T>, Cell, ?> getConverter();

    BiConsumer<T, ?> getSetter();

    String getWrongFormatErrorMessageTemplate();

    String getNullErrorMessageTemplate();

    default boolean isRequired() {
        return null != getNullErrorMessageTemplate();
    }

    default Date toDate(final Cell cell) {
        if (null == cell) {
            return null;
        }
        if (cell.getCellType() == CELL_TYPE_BLANK) {
            return assertIsNotNullAndReturn(null);
        }
        if (cell.getCellType() == CELL_TYPE_NUMERIC && HSSFDateUtil.isCellDateFormatted(cell)) {
            return assertIsNotNullAndReturn(cell.getDateCellValue());
        }
        throw getException(cell, "date");
    }

    default int toInt(final Cell cell) {
        if (null == cell) {
            return 0;
        }
        if (cell.getCellType() == CELL_TYPE_BLANK) {
            return assertIsNotNullAndReturn(null);
        }
        if (cell.getCellType() == CELL_TYPE_NUMERIC && !HSSFDateUtil.isCellDateFormatted(cell)) {
            int a = Math.toIntExact(Math.round(cell.getNumericCellValue()));
            if ((double) a == cell.getNumericCellValue()) {
                return assertIsNotNullAndReturn(a);
            }
            throw getException(cell, "int");
        }
        throw getException(cell, "int");
    }

    default BigDecimal toBigDecimal(final Cell cell) {
        if (null == cell) {
            return null;
        }
        if (cell.getCellType() == CELL_TYPE_BLANK) {
            return assertIsNotNullAndReturn(null);
        }
        if (cell.getCellType() == CELL_TYPE_NUMERIC && !HSSFDateUtil.isCellDateFormatted(cell)) {
            return assertIsNotNullAndReturn(BigDecimal.valueOf(Math.round(cell.getNumericCellValue())));
        }
        throw getException(cell, "int");
    }

    default Salutation toSalutation(final Cell cell) {
        if (null == cell) {
            return null;
        }
        if (cell.getCellType() == CELL_TYPE_BLANK) {
            return assertIsNotNullAndReturn(null);
        }
        if (cell.getCellType() == CELL_TYPE_STRING) {
            return assertIsNotNullAndReturn(Salutation.findByName(cell.getStringCellValue().trim()));
        }
        throw getException(cell, "Geschlecht");
    }

    default String toStringFromStringOnly(final Cell cell) {
        if (null == cell) {
            return null;
        }
        switch (cell.getCellType()) {
            case CELL_TYPE_BLANK:
                return assertIsNotNullAndReturn(null);
            case CELL_TYPE_STRING:
                return assertIsNotEmptyAndReturn(cell.getStringCellValue().trim());
        }
        throw getException(cell, "String");
    }

    default String toStringg(final Cell cell) {
        if (null == cell) {
            return null;
        }
        switch (cell.getCellType()) {
            case CELL_TYPE_BLANK:
                return assertIsNotNullAndReturn(null);
            case CELL_TYPE_STRING:
                return assertIsNotEmptyAndReturn(cell.getStringCellValue().trim());
            case CELL_TYPE_NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    return assertIsNotEmptyAndReturn(DateUtils.toGermanDateString(cell.getDateCellValue()));
                } else {
                    DecimalFormat df = new DecimalFormat();
                    df.setMaximumFractionDigits(125);
                    df.setMaximumIntegerDigits(124);
                    df.setMinimumFractionDigits(0);
                    df.setGroupingUsed(false);
                    df.setParseBigDecimal(true);
                    return assertIsNotEmptyAndReturn(df.format(cell.getNumericCellValue()));
                }
        }
        return assertIsNotEmptyAndReturn(new DataFormatter().formatCellValue(cell));
    }

    default void convertCellValueAndSetValueInEntity(T a, Cell b) {
        final Object value = convertFromCell(b);
        getSetterCasted().accept(a, value);
    }

    default Object convertFromCell(Cell b) {
        return getConverter().apply(ExcelImportColumn.this, b);
    }

    default BiConsumer<T, Object> getSetterCasted() {
        return (BiConsumer) getSetter();
    }

    static String getCellType(Cell cell) {
        switch (cell.getCellType()) {
            case CELL_TYPE_BLANK:
                return "Blank";
            case CELL_TYPE_BOOLEAN:
                return "Boolean";
            case CELL_TYPE_ERROR:
                return "Error";
            case CELL_TYPE_FORMULA:
                return "Formula";
            case CELL_TYPE_NUMERIC:
                return "Numeric";
            case CELL_TYPE_STRING:
                return "String";
            default:
                return "Unknown";
        }
    }

    default String assertIsNotEmptyAndReturn(String object) {
        assertIsNotNullAndReturn(object);
        if (isRequired() && object.isEmpty()) {
            throw new GPDOAServiceException(getNullErrorMessageTemplate());
        }
        return object;
    }

    default <O> O assertIsNotNullAndReturn(O object) {
        if (isRequired() && null == object) {
            throw new GPDOAServiceException(getNullErrorMessageTemplate());
        }
        return object;
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    default GPDOAServiceException getException(Cell cell, final String expectedType) {
        String template = getWrongFormatErrorMessageTemplate();
        return null == template
                ? getGenericColumnException(cell, expectedType)
                : getNamedColumnException(cell, template);
    }

    default GPDOAServiceException getGenericColumnException(Cell cell, String expectedType) {
        return new GPDOAServiceException("Wrong type of excel cell at column: " + getColumnName()
                + ". Expected: " + expectedType + " Found: " + getCellType(cell));
    }

    default GPDOAServiceException getNamedColumnException(Cell cell, String template) {
        HashMap<String, String> map = Maps.newHashMap();
        final String formatCellValue = new DataFormatter().formatCellValue(cell);
        map.put("value", formatCellValue.isEmpty() ? "N/A" : formatCellValue);
        String output = StrSubstitutor.replace(template, map);
        return new GPDOAServiceException(output);
    }
}
