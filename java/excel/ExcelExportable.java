public interface ExcelExportable<T extends ExcelExportable<T>> {

    ExcelExportColumn<T>[] getExcelExportColumns();
}
