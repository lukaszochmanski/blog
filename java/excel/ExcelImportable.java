import org.apache.poi.ss.usermodel.Cell;

public interface ExcelImportable<T extends ExcelImportable<T>> {

    ExcelImportColumn<T>[] getExcelImportColumns();

    void convertCellValueAndCallAppropriateSetter(ExcelImportColumn<T> a, Cell b);
}
