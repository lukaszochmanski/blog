import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;

public final class ReflectionUtils {

    private ReflectionUtils() {
    }

    public static List<Field> getAllFields(Class<?> type) {
        return getAllFieldsRecursively(new LinkedList<>(), type);
    }

    private static List<Field> getAllFieldsRecursively(List<Field> fields, Class<?> type) {
        List<Field> c = Stream.of(type.getDeclaredFields()).filter(not(Field::isSynthetic)).collect(Collectors.toList());
        fields.addAll(c);
        return type.getSuperclass() == null ? fields : getAllFieldsRecursively(fields, type.getSuperclass());
    }

    public static List<Field> getAllFieldsAnnotatedWith(Class<? extends Annotation> annotationClass, Class<?> aClass) {
        return getAllFields(aClass).stream()
                .filter(p -> p.isAnnotationPresent(annotationClass))
                .collect(toList());
    }

    public static <T> Predicate<T> not(Predicate<T> t) {
        return t.negate();
    }

    public static Set<Field> getAllNonSynteticFields(Class<?> aClass, com.google.common.base.Predicate... predicates) {
        Set<Field> allFields = org.reflections.ReflectionUtils.getAllFields(aClass, predicates);
        return allFields.stream()
                .filter(not(Field::isSynthetic))
                .collect(Collectors.toSet());
    }
}
