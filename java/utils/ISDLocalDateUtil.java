package com.basf.gpdoa.base.util;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public interface ISDLocalDateUtil {

    LocalDate today();

    LocalDate of(int year, Month month, int dayOfMonth);

    LocalDate from(Date date);

    Date from(LocalDate localDate);

    Date getDate(String dateAsString);

    Date getDate(long dateAsLong);

    LocalDate getLocalDate(String dateAsString);

    LocalDate getLocalDate(long dateAsLong);

    String format(LocalDate value);

    String format(Date value);

    String formatDateEmptyIfNull(Date date);

    String thisYear();

    long getLong(LocalDate localDate);
}
