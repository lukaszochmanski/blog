package com.basf.gpdoa.base.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;

public interface ISDLocalDateTimeUtil {

    LocalDateTime now();

    LocalDateTime of(int year, Month month, int dayOfMonth, int hour, int minute);

    LocalDateTime from(Date date);

    Date from(LocalDateTime localDate);

    LocalDateTime getLocalDateTime(String dateAsString);

    LocalDateTime getLocalDateTime(long dateAsLong);

    String formatDateAndTime(LocalDateTime value);

    String formatDateAndTime(Date value);

    String formatTimeWithSeconds(Date value);

    LocalDateTime of(int year, Month month, int dayOfMonth);

    String formatDate(Date value);

    String formatTime(Date value);

    String formatDate(LocalDate value);

    long getLong(LocalDateTime value);
}
