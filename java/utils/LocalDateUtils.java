package com.basf.gpdoa.base.util;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class LocalDateUtils {

    private static ISDLocalDateUtil defaultImplementation = new ISDLocalDateUtilImpl();

    public static ISDLocalDateUtil setDefaultImplementation(ISDLocalDateUtil isdLocalDateUtil) {
        return defaultImplementation = isdLocalDateUtil;
    }

    public static LocalDate today() {
        return defaultImplementation.today();
    }

    public static LocalDate of(int year, Month month, int dayOfMonth) {
        return defaultImplementation.of(year, month, dayOfMonth);
    }

    public static LocalDate from(Date date) {
        return defaultImplementation.from(date);
    }

    public static Date from(LocalDate localDate) {
        return defaultImplementation.from(localDate);
    }

    public static Date getDate(String dateAsString) {
        return defaultImplementation.getDate(dateAsString);
    }

    public static Date getDate(long dateAsLong) throws ParseException {
        return defaultImplementation.getDate(dateAsLong);
    }

    public static LocalDate getLocalDate(String dateAsString) {
        return defaultImplementation.getLocalDate(dateAsString);
    }

    public static LocalDate getLocalDate(long dateAsLong) {
        return defaultImplementation.getLocalDate(dateAsLong);
    }

    public static String format(LocalDate value) {
        return defaultImplementation.format(value);
    }

    public static String format(Date value) {
        return defaultImplementation.format(value);
    }

    public static String formatDateEmptyIfNull(Date date) {
        return defaultImplementation.formatDateEmptyIfNull(date);
    }

    public static String thisYear() {
        return defaultImplementation.thisYear();
    }

    public static long getLong(LocalDate localDate) {
        return defaultImplementation.getLong(localDate);
    }

}
