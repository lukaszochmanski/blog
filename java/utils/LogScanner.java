mport java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class LogScanner {

    private static final int LIMIT = 300;

    private List<String> include;

    private List<String> exclude;

    public LogScanner() {
        try {
            include = Files.readAllLines(Paths.get("include.txt"));
            exclude = Files.readAllLines(Paths.get("exclude.txt"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static void main(String... args) {
        new LogScanner().scan(args);
    }

    private void scan(String... paths) {
        for (String s : paths) {
            scan(Path.of(s));
        }
    }

    private void scan(Path p) {
        try {
            Iterator<String> iterator = Files.readAllLines(p).iterator();
            while (iterator.hasNext()) {
                String s = iterator.next();
                if (contains(s)) {
                    if (doesNotContain(s)) {
                        System.out.println(limit(s));
                        System.out.println(limit(iterator.next()));
                        System.out.println(limit(iterator.next()));
                        System.out.println(limit(iterator.next()));
                        System.out.println(limit(iterator.next()));
                        System.out.println();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private boolean contains(String line) {
        for (String s : include) {
            if (line.contains(s)) {
                return true;
            }
        }
        return false;
    }

    private boolean doesNotContain(String line) {
        for (String s : exclude) {
            if (line.contains(s)) {
                return false;
            }
        }
        return true;
    }

    private String limit(String line) {
        int limit = line.length() >= LIMIT ? LIMIT : line.length();
        return line.substring(0, limit);
    }
}
