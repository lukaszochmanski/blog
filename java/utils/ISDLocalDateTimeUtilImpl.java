package com.basf.gpdoa.base.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Locale;

public class ISDLocalDateTimeUtilImpl implements ISDLocalDateTimeUtil {

    private static final String FORMAT_DATE = "dd.MM.yyyy";
    private static final String FORMAT_TIME = "HH:mm";
    private static final String FORMAT_DATE_AND_TIME = FORMAT_DATE + " " + FORMAT_TIME;

    @Override
    public LocalDateTime now() {
        return LocalDateTime.now();
    }

    @Override
    public LocalDateTime of(int year, Month month, int dayOfMonth, int hour, int minute) {
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute);
    }

    @Override
    public LocalDateTime from(Date date) {
        return date == null ? null : LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    @Override
    public Date from(LocalDateTime localDate) {
        return localDate == null ? null : Date.from(toInstant(localDate));
    }

    private Instant toInstant(LocalDateTime localDate) {
        return localDate.atZone(ZoneId.systemDefault()).toInstant();
    }

    @Override
    public LocalDateTime getLocalDateTime(String dateAsString) throws DateTimeParseException {
        if (dateAsString == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_AND_TIME);
        return LocalDateTime.parse(dateAsString, formatter);
    }

    @Override
    public LocalDateTime getLocalDateTime(long dateAsLong) throws DateTimeParseException {
        return from(new Date(dateAsLong));
    }

    @Override
    public String formatDateAndTime(LocalDateTime value) throws DateTimeException {
        if (value == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_AND_TIME);
        return formatter.format(value);
    }

    @Override
    public String formatDateAndTime(Date value) {
        if (value == null) {
            return null;
        }
        LocalDateTime localDateTime = from(value);
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE_AND_TIME);
        return formatter.format(localDateTime);
    }

    @Override
    public String formatDate(Date value) {
        if (value == null) {
            return null;
        }
        final DateFormat formatter = new SimpleDateFormat(FORMAT_DATE, Locale.GERMANY);
        return formatter.format(value);
    }

    @Override
    public String formatTime(final Date value) {
        if (value == null) {
            return null;
        }
        final DateFormat formatter = new SimpleDateFormat(FORMAT_TIME, Locale.GERMANY);
        return formatter.format(value);
    }

    @Override
    public String formatDate(LocalDate value) throws DateTimeException {
        if (value == null) {
            return null;
        }
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE);
        return formatter.format(value);
    }

    @Override
    public long getLong(LocalDateTime value) {
        return null == value ? Long.MIN_VALUE : Timestamp.valueOf(value).getTime();
    }

    @Override
    public String formatTimeWithSeconds(final Date value) {
        if (value == null) {
            return null;
        }
        final DateFormat format = new SimpleDateFormat(FORMAT_TIME + ":SS", Locale.GERMANY);
        return format.format(value);
    }

    @Override
    public LocalDateTime of(int year, Month month, int dayOfMonth) {
        return of(year, month, dayOfMonth, 0, 0);
    }

}
