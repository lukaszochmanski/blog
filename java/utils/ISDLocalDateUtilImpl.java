package com.basf.gpdoa.base.util;

import java.sql.Timestamp;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ISDLocalDateUtilImpl implements ISDLocalDateUtil {

    private static final String FORMAT_DATE = "dd.MM.yyyy";

    @Override
    public LocalDate today() {
        return LocalDate.now();
    }

    @Override
    public LocalDate of(int year, Month month, int dayOfMonth) {
        return LocalDate.of(year, month, dayOfMonth);
    }

    @Override
    public LocalDate from(Date date) {
        if (null == date) {
            return null;
        }
        LocalDateTime localDateTime = toLocalDateTime(date);
        return null == localDateTime ? null : localDateTime.toLocalDate();
    }

    private LocalDateTime toLocalDateTime(Date date) {
        return DateTimeUtils.from(date);
    }

    @Override
    public Date from(LocalDate localDate) {
        return null == localDate ? null : Date.from(toInstant(localDate));
    }

    private Instant toInstant(LocalDate localDate) {
        return localDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant();
    }

    @Override
    public Date getDate(String dateAsString) {
        if (null == dateAsString) {
            return null;
        }
        LocalDate localDate = getLocalDate(dateAsString);
        return localDate == null ? null : from(localDate);
    }

    @Override
    public Date getDate(long dateAsLong) {
        return new Date(dateAsLong);
    }

    @Override
    public LocalDate getLocalDate(String dateAsString) {
        if (null == dateAsString) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_DATE);
        return LocalDate.parse(dateAsString, formatter);
    }

    @Override
    public LocalDate getLocalDate(long dateAsLong) {
        return LocalDateUtils.from(new Date(dateAsLong));
    }

    @Override
    public String format(Date value) {
        return DateTimeUtils.formatDate(value);
    }

    @Override
    public String format(LocalDate value) throws DateTimeException {
        return DateTimeUtils.formatDate(value);
    }

    @Override
    public String thisYear() {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");
        return formatter.format(LocalDateUtils.today());
    }

    @Override
    public long getLong(LocalDate value) {
        return null == value ? Long.MIN_VALUE : Timestamp.valueOf(value.atStartOfDay()).getTime();
    }

    @Override
    public String formatDateEmptyIfNull(Date date) {
        return null == date ? "" : format(date);
    }
}
