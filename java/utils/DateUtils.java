package com.basf.gpdoa.base.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static final String DATE_FORMAT_GERMAN = "dd.MM.yyyy";

    public static Date parseGermanDate(final String dateString) {
        final DateFormat df = new SimpleDateFormat(DATE_FORMAT_GERMAN);
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String toGermanDateString(final Date date) {
        if (null == date) {
            return null;
        }
        final DateFormat df = new SimpleDateFormat(DATE_FORMAT_GERMAN);
        return df.format(date);
    }

    public static Date now() {
        return new Date();
    }
}
