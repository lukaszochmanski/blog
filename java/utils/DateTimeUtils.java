package com.basf.gpdoa.base.util;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class DateTimeUtils {

    private static ISDLocalDateTimeUtil defaultImplementation = new ISDLocalDateTimeUtilImpl();

    public static void setDefaultImplementation(ISDLocalDateTimeUtil isdLocalDateTimeUtil) {
        defaultImplementation = isdLocalDateTimeUtil;
    }

    public static LocalDateTime now() {
        return defaultImplementation.now();
    }

    public static LocalDateTime of(int year, Month month, int dayOfMonth, int hour, int minute) {
        return defaultImplementation.of(year, month, dayOfMonth, hour, minute);
    }

    public static LocalDateTime of(int year, Month month, int dayOfMonth) {
        return defaultImplementation.of(year, month, dayOfMonth);
    }

    public static LocalDateTime from(Date date) {
        return defaultImplementation.from(date);
    }

    public static Date from(LocalDateTime localDateTime) {
        return defaultImplementation.from(localDateTime);
    }

    public static LocalDateTime getLocalDateTime(String dateAsString) throws DateTimeParseException {
        return defaultImplementation.getLocalDateTime(dateAsString);
    }

    public static LocalDateTime getLocalDateTime(long dateAsLong) throws DateTimeParseException {
        return defaultImplementation.getLocalDateTime(dateAsLong);
    }

    /**
     *
     * @param value
     * @return string in a given format: dd.MM.yyyy HH:mm For example 21.11.2015 12:33 if no time is given then: 21.11.2015 00:00 is returned
     */
    public static String formatDateAndTime(LocalDateTime value) throws DateTimeException {
        return defaultImplementation.formatDateAndTime(value);
    }

    /**
     *
     * @param value
     * @return string in a given format: dd.MM.yyyy HH:mm For example 21.11.2015 12:33 if no time is given then: 21.11.2015 00:00 is returned
     */
    public static String formatDateAndTime(Date value) {
        return defaultImplementation.formatDateAndTime(value);
    }

    /**
     *
     * @param value
     * @return string in a given format: dd.MM.yyyy For example: 21.11.2015 The time part is discarded.
     */
    public static String formatDate(Date value) {
        return defaultImplementation.formatDate(value);
    }

    /**
     *
     * @param value
     * @return string in a given format: dd.MM.yyyy For example: 21.11.2015 The time part is discarded.
     */
    public static String formatDate(LocalDate value) {
        return defaultImplementation.formatDate(value);
    }

    /**
     *
     * @param value
     * @return string in a given format: HH:mm For example: 16:22 If no time is given then 00:00 is returned. The day, month and year is discarded.
     */
    public static String formatTime(Date value) {
        return defaultImplementation.formatTime(value);
    }

    /**
     *
     * @param value
     * @return string in a given format: HH:mm:SS"; For example 16:22:59 The day, month and year is discarded.
     */
    public static String formatTimeWithSeconds(Date value) {
        return defaultImplementation.formatTimeWithSeconds(value);
    }

    public static long getLong(LocalDateTime value) {
        return defaultImplementation.getLong(value);
    }
}
