import com.basf.gpdoa.base.excel.ExcelExportColumn;
import static com.basf.gpdoa.base.excel.ExcelExportColumn.toStringg;
import com.basf.gpdoa.base.excel.ExcelExportable;
import com.basf.gpdoa.base.model.entity.BaseEntity;
import com.basf.gpdoa.base.model.entity.Color;
import com.basf.gpdoa.base.model.entity.Colored;
import static com.basf.gpdoa.base.util.ReflectionUtils.not;
import com.basf.gpdoa.injob.model.dataholder.AreaGroupPlatformEmployee;
import com.basf.gpdoa.injob.model.entity.Employee.ReasonForNotEmploying;
import com.basf.gpdoa.injob.model.entity.EmployeeMasterData.EmployeeSearchView;
import com.basf.gpdoa.injob.model.entity.EmployeeProfile.EmployeeProfileSaveView;
import com.basf.gpdoa.injob.model.entity.PlatformEmployee.PlatformEmployeeDetailsView;
import com.basf.gpdoa.injob.model.entity.PlatformEmployee.PlatformEmployeesView;
import com.basf.gpdoa.injob.model.entity.validator.AreaGroupEmployeeValidator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Lists;
import com.querydsl.core.annotations.QueryInit;
import de.isd.commons.springframework.jsonview.PageView;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;



@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "InjobAreaGroupEmployee")
@Table(schema = "INJOB", name = "AREA_GROUP_EMPLOYEES",
    indexes = {
        @Index(name = "PK_AREA_GROUP_EMPLOYEE", columnList = "ID", unique = true),
        @Index(name = "UNIQUE_AREA_GROUP_EMPLOYEES_CANDIDATE", columnList = "CANDIDATE", unique = true),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_CREATED_BY", columnList = "CREATED_BY"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_LAST_CHANGED", columnList = "LAST_CHANGED"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_LAST_CHANGED_BY", columnList = "LAST_CHANGED_BY"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_STATUS", columnList = "STATUS"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_CREATED", columnList = "CREATED"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_DEACTIVATED", columnList = "DEACTIVATED"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_DEACTIVATION_DATE", columnList = "DEACTIVATION_DATE"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_DEACTIVATED_BY", columnList = "DEACTIVATED_BY"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_DEACTIVATION_REASON", columnList = "DEACTIVATION_REASON"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_FROZEN_BY", columnList = "FROZEN_BY"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_FROZEN", columnList = "FROZEN"),
        @Index(name = "IDX_AREA_GROUP_EMPLOYEES_TRANSFER_PREFERRED", columnList = "TRANSFER_PREFERRED")
    })
@AuditTable("AREA_GROUP_EMPLOYEES_AUD")
public class AreaGroupEmployee extends BaseEntity<AreaGroupEmployee> implements ExcelExportable<AreaGroupEmployee>, AreaGroupEmployeeValidator, AreaGroupPlatformEmployee, Colored {

    // <editor-fold defaultstate="collapsed" desc="JSON views">
    public interface AreaGroupEmployeesView extends PageView {
    }

    public interface AreaGroupEmployeeDetailsView extends PageView {
    }

    public interface AreaGroupEmployeeDetailsViewFull extends AreaGroupEmployeeDetailsView {
    }

    public interface AreaGroupEmployeeProvideProfileMeetingDateView extends PageView {
    }

    public interface AreaGroupEmployeeUploadSignedProfileView extends PageView {
    }

    public interface AreaGroupEmployeeQualificationConsultingDeclineView extends PageView {
    }

    public interface AreaGroupEmployeeProgressPlansSaveView extends PageView {
    }

    public interface AreaGroupEmployeeQualificationConsultingChecklistSaveView extends AreaGroupEmployeeProgressPlansSaveView {
    }

    public interface AreaGroupEmployeeDeclineProgressPlanView extends PageView {
    }

    public interface AreaGroupEmployeeSaveProgressPlanProgressView extends PageView {
    }

    public interface AreaGroupEmployeeCancelProgressPlanView extends PageView {
    }

    public interface AreaGroupEmployeeExtendAreaGroupMembershipView extends PageView {
    }

    public interface AreaGroupEmployeeProvideTransferPreferenceView extends PageView {
    }

    public interface AreaGroupEmployeePlatformAcceptView extends PageView {
    }

    public interface AreaGroupEmployeePlatformDeclineView extends PageView {
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ExcelExportColumns">
    @Getter
    @AllArgsConstructor
    private enum ExcelExportColumns implements ExcelExportColumn<AreaGroupEmployee> {

        SAP_EMPLOYEE_NUMBER(0, AreaGroupEmployee::getSapEmployeeNumber, 18, "Personalnummer"),
        FIRST_NAME(1, AreaGroupEmployee::getFirstName, 20, "Vorname"),
        LAST_NAME(2, AreaGroupEmployee::getLastName, 20, "Nachname"),
        BIRTHDAY(3, a -> toStringg(a.getBirthday()), 18, "Geburtsdatum"),
        ORG_CODE(4, AreaGroupEmployee::getOrgCode, 20, "Organisationscode"),
        COST_CENTER(5, AreaGroupEmployee::getCostCenter, 20, "Kostenstelle"),
        SECTOR(6, AreaGroupEmployee::getSectorName, 12, "Bereich"),
        HRBP(7, AreaGroupEmployee::getHrbpName, 22, "HR-Business-Partner"),
        BUILDING(8, AreaGroupEmployee::getBuilding, 12, "Bau"),
        PAYMENT_GROUP(9, AreaGroupEmployee::getPaymentGroup, 17, "Entgeltgruppe"),
        PAYMENT_LEVEL(10, AreaGroupEmployee::getPaymentLevel, 17, "Entgeltstufe"),
        SUPERVISOR(11, AreaGroupEmployee::getSupervisor, 22, "Führungskraft"),
        PROFILE(12, AreaGroupEmployee::getProfileNameAndType, 50, "Tätigkeitsbezeichnung"),
        APPRENTICESHIP(13, AreaGroupEmployee::getApprenticeship, 50, "Ausbildung / Beruf"),
        AVAILABLE_FROM(14, a -> toStringg(a.getAvailableFrom()), 18, "Verfügbar ab"),
        REASON_FOR_NOT_EMPLOYING(15, a -> toStringg(a.getReasonForNotEmploying()), 30, "Grund der Meldung"),
        TEAM_LEADER(16, AreaGroupEmployee::getTeamLeaderName, 22, "Fachteam-Leiter"),
        TRANSFER_DATE_AREA_GROUP(17, a -> toStringg(a.getTransferDateAreaGroup()), 40, "Übernahmedatum (Bereichsgruppe)"),
        TRANSFER_DATE(18, a -> toStringg(a.getTransferDate()), 35, "Übernahmedatum (Plattform)");

        private final int columnIndex;
        private final Function<AreaGroupEmployee, String> getter;
        private final int columnWidth;
        private final String headline;

        @Override
        public String getValue(final AreaGroupEmployee areaGroupEmployee) {
            return this.getter.apply(areaGroupEmployee);
        }
    }
    // </editor-fold>

    // TODO ROBIN: FIX ENUM CONSTANTS NUMBERS
    // <editor-fold defaultstate="collapsed" desc="Status">
    public enum Status {

        /**
         * Es muss gewartet werden, bis das Übernahmedatum eingetreten ist
         */
        S01_PENDING,

        /**
         * Das Übernahmedatum ist eingetreten und der Mitarbeiter ist nun aktiv in der Bereichsgruppe
         */
        S03_TRANSFER_DATE_PASSED,

        /**
         * STAFF hat einen Termin für das Erstgespräch hinterlegt
         */
        S04_PROFILE_MEETING_DATE_PROVIDED,

        /**
         * STAFF hat die Arbeitsunterlage zwischengespeichert
         */
        S05_PROFILE_PENDING,

        /**
         * STAFF hat die Arbeitsunterlage final gespeichert
         */
        S06_PROFILE_FINALIZED,

        /**
         * STAFF die ausgefüllte Arbeitsunterlage zum Unterschreiben ausgedruckt
         */
        S07_PROFILE_DOWNLOADED,

        /**
         * STAFF hat die unterschriebene Arbeitsunterlage hochgeladen
         */
        S08_PROFILE_SIGNED,

        /**
         * HRBP hat die erforderliche Qualifizierungsberatung akzeptiert
         */
        S08_QUALIFICATION_CONSULTING_ACCEPTED,

        /**
         * Fall 1: STAFF (Qualifizierungsteam) hat die Checkliste der Qualifizierungsberatung ausgefüllt, nachdem die Qualifizierungsberatung vom HRBP akzeptiert wurde
         * Fall 2: HRBP hat die erforderliche Qualifizierungsberatung abgelehnt
         */
        S09_PROFILE_CHECKLIST_SAVED,

        /**
         * STAFF hat einen oder mehrere Maßnahmenpläne bereitgestellt - der HRBP muss diese nun akzeptieren oder ablehnen
         */
        S10_PROGRESS_PLANS_PENDING,

        /**
         * Der Mitarbeiter ist seit mindestens 9 Monaten in der Bereichsgruppe - nun kann entschieden werden,
         * ob man die Bereichsgruppenzugehörigkeit verlängern oder eine Übernahme in die Plattform vorziehen möchte
         */
        S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON,

        /**
         * Der Mitarbeiter kann in die Plattform übernommen werden
         */
        S12_CAN_BE_ACCEPTED_FOR_PLATFORM,

        /**
         * PLATFORM LEADER hat seinen Übernahmewunsch hinterlegt
         */
        S13_TRANSFER_PREFERENCE_PROVIDED,

        /**
         * WORKS COUNCIL PLATFORM hat den Mitarbeiter in die Plattform übernommen
         */
        S14_PLATFORM_ACCEPTED,

        /**
         * WORKS COUNCIL PLATFORM hat den Mitarbeiter nicht in die Plattform übernommen (ihn abgelehnt)
         */
        S14_PLATFORM_DECLINED
    }
    // </editor-fold>

    @Version
    private Long version;

    @CreatedDate
    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "CREATED", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @CreatedBy
    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    @ManyToOne
    @JoinColumn(name = "CREATED_BY" /*, nullable = false*/, referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_CREATED_BY"))
    private InjobUser createdBy;

    @LastModifiedDate
    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    @Column(name = "LAST_CHANGED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChanged;

    @LastModifiedBy
    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    @ManyToOne
    @JoinColumn(name = "LAST_CHANGED_BY", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_LAST_CHANGED_BY"))
    private InjobUser lastChangedBy;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsViewFull.class})
    @Column(name = "FROZEN", nullable = false)
    @Audited
    private boolean frozen;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsViewFull.class})
    @ManyToOne
    @JoinColumn(name = "FROZEN_BY", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_FROZEN_BY"))
    private InjobUser frozenBy;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsViewFull.class})
    @Column(name = "FREEZE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @Audited
    private Date freezeDate;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsViewFull.class})
    @Column(name = "FREEZE_COMMENT")
    @Audited
    private String freezeComment;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, EmployeeSearchView.class})
    @Column(name = "DEACTIVATED", nullable = false)
    @Audited
    private boolean deactivated;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    @ManyToOne
    @JoinColumn(name = "DEACTIVATED_BY", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_DEACTIVATED_BY"))
    private InjobUser deactivatedBy;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    @Column(name = "DEACTIVATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @Audited
    private Date deactivationDate;

    @JsonView(AreaGroupEmployeeDetailsView.class)
    @ManyToOne
    @JoinColumn(name = "DEACTIVATION_REASON", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_DEACTIVATION_REASON"))
    private DeactivationReason deactivationReason;

    @JsonView({EmployeeSearchView.class, AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, PlatformEmployeeDetailsView.class})
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    @Audited
    private Status status;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, EmployeeProfileSaveView.class, AreaGroupEmployeeUploadSignedProfileView.class, AreaGroupEmployeeQualificationConsultingDeclineView.class, AreaGroupEmployeeQualificationConsultingChecklistSaveView.class, AreaGroupEmployeeProgressPlansSaveView.class, PlatformEmployeesView.class, PlatformEmployeeDetailsView.class, AreaGroupEmployeeMediationAttempt.AreaGroupEmployeeMediationAttemptView.class})
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CANDIDATE", referencedColumnName = "ID" /*, nullable = false*/, foreignKey = @ForeignKey(name = "FK_AREA_GROUP_EMPLOYEES_CANDIDATE"))
    @QueryInit({"employee.sector", "employee.employeeProfile.qualificationConsultingRequired"})
    private Candidate candidate;

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, AreaGroupEmployeeProvideProfileMeetingDateView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "PROGRESS_PLAN_MEETING_DATE")
    @Temporal(TemporalType.DATE)
    @Audited
    private Date progressPlanMeetingDate;

    @JsonView({AreaGroupEmployeeDetailsView.class, EmployeeProfileSaveView.class, AreaGroupEmployeeProgressPlansSaveView.class, AreaGroupEmployeeDeclineProgressPlanView.class, AreaGroupEmployeeSaveProgressPlanProgressView.class, AreaGroupEmployeeCancelProgressPlanView.class, PlatformEmployeeDetailsView.class})
    @OneToMany(mappedBy = "areaGroupEmployee", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<AreaGroupEmployeeProgressPlan> progressPlans = Lists.newArrayList();

    @JsonView({AreaGroupEmployeeDetailsView.class, EmployeeProfileSaveView.class, AreaGroupEmployeeProgressPlansSaveView.class, PlatformEmployeeDetailsView.class})
    @OneToMany(mappedBy = "areaGroupEmployee", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<AreaGroupEmployeeMediationAttempt> mediationAttempts = Lists.newArrayList();

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class, AreaGroupEmployeeExtendAreaGroupMembershipView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "AREA_GROUP_MEMBERSHIP_END")
    @Temporal(TemporalType.TIMESTAMP)
    private Date areaGroupMembershipEnd;

    @JsonView({AreaGroupEmployeeDetailsView.class, AreaGroupEmployeeProvideTransferPreferenceView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "TRANSFER_PREFERRED")
    private Boolean transferPreferred;

    @JsonView({AreaGroupEmployeeDetailsView.class, AreaGroupEmployeePlatformAcceptView.class, AreaGroupEmployeePlatformDeclineView.class, PlatformEmployeeDetailsView.class})
    @OneToMany(mappedBy = "areaGroupEmployee", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private List<AreaGroupEmployeeMeetingProtocol> meetingProtocols = Lists.newArrayList();

    @JsonView({AreaGroupEmployeeDetailsView.class, AreaGroupEmployeePlatformDeclineView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "TRANSFER_COMMENT")
    @Audited
    private String transferComment;

    @JsonView({AreaGroupEmployeeDetailsView.class, AreaGroupEmployeePlatformAcceptView.class, PlatformEmployeeDetailsView.class})
    @Column(name = "TRANSFER_DATE")
    @Temporal(TemporalType.DATE)
    @Audited
    private Date transferDate;

    @JsonView(AreaGroupEmployeesView.class)
    @Transient
    private Color color;

    @JsonView(AreaGroupEmployeesView.class)
    @Transient
    private String rowDescription;

    // <editor-fold defaultstate="collapsed" desc="convenience methods">
    @JsonView({AreaGroupEmployeeDetailsView.class, PlatformEmployeeDetailsView.class})
    public List<AreaGroupEmployeeProgressPlan> getActiveProgressPlans() {
        return progressPlans.stream().filter(AreaGroupEmployeeProgressPlan::isActive).collect(Collectors.toList());
    }

    @JsonView({AreaGroupEmployeeDetailsView.class, PlatformEmployeeDetailsView.class})
    public List<AreaGroupEmployeeProgressPlan> getInactiveProgressPlans() {
        return progressPlans.stream().filter(not(AreaGroupEmployeeProgressPlan::isActive)).collect(Collectors.toList());
    }

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    public long getAmountOfProgressPlansNotSentToHrbpYet() {
        return getActiveProgressPlans().stream().filter(not(AreaGroupEmployeeProgressPlan::isSentToHrbp)).count();
    }

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    public long getAmountOfAcceptableProgressPlansNotYetAccepted() {
        return getActiveProgressPlans().stream().filter(AreaGroupEmployeeProgressPlan::isSentToHrbp).filter(not(AreaGroupEmployeeProgressPlan::isAccepted)).count();
    }

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    public long getAmountOfRunningProgressPlans() {
        return getActiveProgressPlans().stream().filter(AreaGroupEmployeeProgressPlan::isRunning).count();
    }

    @JsonView({AreaGroupEmployeesView.class, AreaGroupEmployeeDetailsView.class})
    public long getAmountOfFinishedProgressPlans() {
        return getInactiveProgressPlans().stream().filter(AreaGroupEmployeeProgressPlan::isAccepted).count();
    }

    @Override
    @JsonIgnore
    public ExcelExportColumn<AreaGroupEmployee>[] getExcelExportColumns() {
        return AreaGroupEmployee.ExcelExportColumns.values();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="excel export getters">
    @JsonIgnore
    @Override
    public String getSapEmployeeNumber() {
        return candidate.getSapEmployeeNumber();
    }

    @JsonIgnore
    @Override
    public String getFirstName() {
        return candidate.getFirstName();
    }

    @JsonIgnore
    @Override
    public String getLastName() {
        return candidate.getLastName();
    }

    @JsonIgnore
    @Override
    public Date getBirthday() {
        return candidate.getBirthday();
    }

    @JsonIgnore
    @Override
    public String getOrgCode() {
        return candidate.getOrgCode();
    }

    @JsonIgnore
    @Override
    public String getCostCenter() {
        return candidate.getCostCenter();
    }

    @JsonIgnore
    public String getSectorName() {
        return candidate.getSectorName();
    }

    @JsonIgnore
    @Override
    public String getHrbpName() {
        return candidate.getHrbpName();
    }

    @JsonIgnore
    @Override
    public String getBuilding() {
        return candidate.getBuilding();
    }

    @JsonIgnore
    public String getPaymentGroup() {
        return candidate.getPaymentGroup();
    }

    @JsonIgnore
    public String getPaymentLevel() {
        return candidate.getPaymentLevel();
    }

    @JsonIgnore
    @Override
    public String getSupervisor() {
        return candidate.getSupervisor();
    }

    @JsonIgnore
    public String getProfileNameAndType() {
        return candidate.getProfileNameAndType();
    }

    @JsonIgnore
    @Override
    public String getApprenticeship() {
        return candidate.getApprenticeship();
    }

    @JsonIgnore
    @Override
    public Date getAvailableFrom() {
        return candidate.getAvailableFrom();
    }

    @JsonIgnore
    @Override
    public ReasonForNotEmploying getReasonForNotEmploying() {
        return candidate.getReasonForNotEmploying();
    }

    @JsonIgnore
    public String getTeamLeaderName() {
        return candidate.getTeamLeaderName();
    }

    @JsonIgnore
    public Date getTransferDateAreaGroup() {
        return candidate.getTransferDate();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="useful delegators">
    @JsonIgnore
    public InjobUser getHrbp() {
        return candidate.getHrbp();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fluent setters">
    public AreaGroupEmployee version(final Long version) {
        this.version = version;
        return this;
    }

    public AreaGroupEmployee created(final Date created) {
        this.created = created;
        return this;
    }

    public AreaGroupEmployee createdBy(final InjobUser createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public AreaGroupEmployee lastChanged(final Date lastChanged) {
        this.lastChanged = lastChanged;
        return this;
    }

    public AreaGroupEmployee lastChangedBy(final InjobUser lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
        return this;
    }

    public AreaGroupEmployee frozen(final boolean frozen) {
        this.frozen = frozen;
        return this;
    }

    public AreaGroupEmployee frozenBy(final InjobUser frozenBy) {
        this.frozenBy = frozenBy;
        return this;
    }

    public AreaGroupEmployee freezeDate(final Date freezeDate) {
        this.freezeDate = freezeDate;
        return this;
    }

    public AreaGroupEmployee freezeComment(final String freezeComment) {
        this.freezeComment = freezeComment;
        return this;
    }

    public AreaGroupEmployee deactivated(final boolean deactivated) {
        this.deactivated = deactivated;
        return this;
    }

    public AreaGroupEmployee deactivatedBy(final InjobUser deactivatedBy) {
        this.deactivatedBy = deactivatedBy;
        return this;
    }

    public AreaGroupEmployee deactivationDate(final Date deactivationDate) {
        this.deactivationDate = deactivationDate;
        return this;
    }

    public AreaGroupEmployee deactivationReason(final DeactivationReason deactivationReason) {
        this.deactivationReason = deactivationReason;
        return this;
    }

    public AreaGroupEmployee status(final Status status) {
        this.status = status;
        return this;
    }

    public AreaGroupEmployee candidate(final Candidate candidate) {
        this.candidate = candidate;
        return this;
    }

    public AreaGroupEmployee progressPlanMeetingDate(final Date progressPlanMeetingDate) {
        this.progressPlanMeetingDate = progressPlanMeetingDate;
        return this;
    }

    public AreaGroupEmployee progressPlans(final List<AreaGroupEmployeeProgressPlan> progressPlans) {
        this.progressPlans = progressPlans;
        return this;
    }

    public AreaGroupEmployee mediationAttempts(final List<AreaGroupEmployeeMediationAttempt> mediationAttempts) {
        this.mediationAttempts = mediationAttempts;
        return this;
    }

    public AreaGroupEmployee areaGroupMembershipEnd(final Date areaGroupMembershipEnd) {
        this.areaGroupMembershipEnd = areaGroupMembershipEnd;
        return this;
    }

    public AreaGroupEmployee transferPreferred(final Boolean transferPreferred) {
        this.transferPreferred = transferPreferred;
        return this;
    }

    public AreaGroupEmployee meetingProtocols(final List<AreaGroupEmployeeMeetingProtocol> meetingProtocols) {
        this.meetingProtocols = meetingProtocols;
        return this;
    }

    public AreaGroupEmployee transferComment(final String transferComment) {
        this.transferComment = transferComment;
        return this;
    }

    public AreaGroupEmployee transferDate(final Date transferDate) {
        this.transferDate = transferDate;
        return this;
    }

    public AreaGroupEmployee color(final Color color) {
        this.color = color;
        return this;
    }

    public AreaGroupEmployee rowDescription(final String rowDescription) {
        this.rowDescription = rowDescription;
        return this;
    }

    // </editor-fold>
}
