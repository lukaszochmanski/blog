package jpa.model;

import lombok.Data;
import org.apache.commons.lang3.builder.EqualsBuilder;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@Data
@MappedSuperclass
public abstract class BaseEntity<T extends BaseEntity<T>> implements Serializable {

    private static final long serialVersionUID = -2936980793563826967L;

    @Id
    @Column(name = "ID", columnDefinition = "NUMBER(10,0)", nullable = false)
    private int id;

    // <editor-fold defaultstate="collapsed" desc="overwritten object methods">
    public boolean isPersisted() {
        return 0 != id;
    }

    @SuppressWarnings("unchecked")
    public T getThis() {
        return (T) this;
    }

    @Override
    public String toString() {
        return (id == 0) ? "No ID available. HashCode: " + super.hashCode()
                : this.getClass() + "{" + "id=" + id + "}";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="fluent setters">
    public T id(int id) {
        this.id = id;
        return getThis();
    }
    // </editor-fold>
}
