package com.censhare.lang;

public class Field<T> implements Nameable<Field<T>> {

    private Class<T> type;
    private String name;

    public Field(Class<T> type, String name) {
        this.type = type;
        this.name = name;
    }

    public Class<T> getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    public T newInstance() throws IllegalAccessException, InstantiationException {
        return getType().newInstance();
    }
}
