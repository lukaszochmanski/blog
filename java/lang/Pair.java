public class Pair<T, U> {

    private final T left;
    private final U right;

    private Pair(T left, U right) {
        this.left = left;
        this.right = right;
    }

    public static <T, U> Pair<T, U> of(T first, U second) {
        return new Pair<>(first, second);
    }

    public T getLeft() {
        return left;
    }

    public U getRight() {
        return right;
    }
}
