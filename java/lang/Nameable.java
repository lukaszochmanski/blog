package com.censhare.lang;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

@FunctionalInterface
public interface Nameable<T extends Nameable<T>> {

    String getName();

    static <S extends Nameable<S>> String getNameIfNotNull(final Nameable<S> nameable) {
        return coalesce(nameable);
    }

    /**
     * Evaluates the arguments in order and returns the current value of the first expression that initially does not evaluate to NULL.
     *
     * @param <S>
     * @param args
     * @return
     */
    @SafeVarargs
    static <S> S coalesce(S... args) {
        return null == args ? null : Arrays.stream(args).filter(Objects::nonNull).findFirst().orElse(null);
    }

    @SafeVarargs
    static <S extends Nameable<S>> String coalesce(Nameable<S>... args) {
        return null == args
                ? null
                : Arrays.stream(args)
                .filter(Objects::nonNull)
                .map(Nameable::getName)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    static <S extends Enum<S> & Nameable<S>> S findByName(Class<S> clazz, String s) {
        return find(clazz, getPredicate(s));
    }

    static <S extends Enum<S> & Nameable<S>> Predicate<S> getPredicate(String s) {
        return p -> p.getName().equalsIgnoreCase(s);
    }

    static <S extends Enum<S> & Nameable<S>> S find(Class<S> clazz, Predicate<S> predicate) {
        return Stream.of(clazz.getEnumConstants()).filter(predicate).findAny().orElse(null);
    }

    static <S> CollectionValidator<S> element(S element) {
        return new CollectionValidator<>(element);
    }

    class CollectionValidator<S> {

        private final S element;

        public CollectionValidator(S element) {
            this.element = element;
        }

        public boolean isIn(S... args) {
            return Lists.newArrayList(args).contains(element);
        }

        public boolean isNotIn(S... args) {
            return !isIn(args);
        }

        public boolean isIn(Collection<? extends S> args) {
            return Lists.newArrayList(args).contains(element);
        }

        public boolean isNotIn(Collection<? extends S> args) {
            return !isIn(args);
        }
    }
}

