import static java.lang.Math.multiplyExact;

public class Byte {

    public static final int SI_THOUSAND = 1000;
    public static final int BINARY_THOUSAND = 1024; //2^10
    public static final int KILOBYTES = BINARY_THOUSAND;
    public static final int ONE_MEGABYTE = KILOBYTES * BINARY_THOUSAND;
    public static final int MEGABYTES = ONE_MEGABYTE;
    public static final long GIGABYTES = MEGABYTES * BINARY_THOUSAND;

    public static int gigabytes(int i) {
        return multiplyExact(BINARY_THOUSAND, multiplyExact(multiplyExact(BINARY_THOUSAND, BINARY_THOUSAND), i));
    }

    public static int megabytes(int i) {
        return multiplyExact(multiplyExact(BINARY_THOUSAND, BINARY_THOUSAND), i);
    }

    public static int kilobytes(int i) {
        return multiplyExact(BINARY_THOUSAND, i);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? SI_THOUSAND : BINARY_THOUSAND;
        if (bytes < unit) {
            return bytes + " B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
