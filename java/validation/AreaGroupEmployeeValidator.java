public interface AreaGroupEmployeeValidator {

    @IsAreaGroupEmployeeInStatus.List({
        @IsAreaGroupEmployeeInStatus(
                values = S03_TRANSFER_DATE_PASSED,
                groups = ProvideProfileMeetingDate.class),
        @IsAreaGroupEmployeeInStatus(
                values = {S04_PROFILE_MEETING_DATE_PROVIDED, S05_PROFILE_PENDING},
                groups = {SaveProfile.class, FinalizeProfile.class}),
        @IsAreaGroupEmployeeInStatus(
                values = {S06_PROFILE_FINALIZED, S07_PROFILE_DOWNLOADED},
                groups = DownloadProfile.class),
        @IsAreaGroupEmployeeInStatus(
                values = S07_PROFILE_DOWNLOADED,
                groups = UploadSignedProfile.class),
        @IsAreaGroupEmployeeInStatus(
                values = S08_PROFILE_SIGNED,
                groups = {AcceptQualificationConsulting.class, DeclineQualificationConsulting.class}),
        @IsAreaGroupEmployeeInStatus(
                values = S08_QUALIFICATION_CONSULTING_ACCEPTED,
                groups = SaveQualificationConsultingChecklist.class),
        @IsAreaGroupEmployeeInStatus(
                values = {S09_PROFILE_CHECKLIST_SAVED, S10_PROGRESS_PLANS_PENDING, S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON},
                groups = {SaveProgressPlans.class, SaveProgressPlansAndInformHrbp.class}),
        @IsAreaGroupEmployeeInStatus(
                values = {S10_PROGRESS_PLANS_PENDING, S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON},
                groups = {AreaGroupEmployeeProgressPlanValidator.AcceptProgressPlan.class,
                    AreaGroupEmployeeProgressPlanValidator.SaveProgressPlanProgress.class,
                    AreaGroupEmployeeProgressPlanValidator.DeclineProgressPlan.class,
                    AreaGroupEmployeeProgressPlanValidator.CancelProgressPlan.class}),
        @IsAreaGroupEmployeeInStatus(
                values = S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON,
                groups = {ExtendAreaGroupMembership.class, EndAreaGroupMembership.class}),
        @IsAreaGroupEmployeeInStatus(
                values = S12_CAN_BE_ACCEPTED_FOR_PLATFORM,
                groups = ProvideTransferPreference.class),
        @IsAreaGroupEmployeeInStatus(
                values = S13_TRANSFER_PREFERENCE_PROVIDED,
                groups = {SaveMeetingDetails.class, AcceptForPlatform.class, DeclineForPlatform.class}),
        @IsAreaGroupEmployeeInStatus(
                values = {S06_PROFILE_FINALIZED, S07_PROFILE_DOWNLOADED, S08_PROFILE_SIGNED, S09_PROFILE_CHECKLIST_SAVED, S10_PROGRESS_PLANS_PENDING, S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON, S12_CAN_BE_ACCEPTED_FOR_PLATFORM, S13_TRANSFER_PREFERENCE_PROVIDED, S14_PLATFORM_ACCEPTED, S14_PLATFORM_DECLINED},
                groups = AdditionalSaveProfile.class),
        @IsAreaGroupEmployeeInStatus(
                values = {S09_PROFILE_CHECKLIST_SAVED, S10_PROGRESS_PLANS_PENDING, S11_CAN_BE_ACCEPTED_FOR_PLATFORM_SOON, S12_CAN_BE_ACCEPTED_FOR_PLATFORM, S13_TRANSFER_PREFERENCE_PROVIDED, S14_PLATFORM_ACCEPTED, S14_PLATFORM_DECLINED},
                groups = AdditionalSaveQualificationConsultingChecklist.class)
    })
    Status getStatus();

    interface ProvideProfileMeetingDate extends Default {
    }

    interface SaveProfile extends Default {
    }

    interface FinalizeProfile extends Default {
    }

    interface DownloadProfile extends Default {
    }

    interface UploadSignedProfile extends Default {
    }

    interface AcceptQualificationConsulting extends Default {
    }

    interface DeclineQualificationConsulting extends Default {
    }

    interface SaveQualificationConsultingChecklist extends Default {
    }

    interface SaveProgressPlans extends Default {
    }

    interface SaveProgressPlansAndInformHrbp extends Default {
    }

    interface ExtendAreaGroupMembership extends Default {
    }

    interface EndAreaGroupMembership extends Default {
    }

    interface ProvideTransferPreference extends Default {
    }

    interface SaveMeetingDetails extends Default {
    }

    interface AcceptForPlatform extends Default {
    }

    interface DeclineForPlatform extends Default {
    }

    interface AdditionalSaveProfile extends Default {
    }

    interface AdditionalSaveQualificationConsultingChecklist extends Default {
    }
}
