package spring.data;


import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringPath;
import com.studiosus.pod.jpa.model.QReiseInfo;
import com.studiosus.pod.jpa.model.ReiseInfo;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

import javax.annotation.Nonnull;
import java.util.List;

public interface ReiseInfoRepository extends QuerydslStreamSupport<ReiseInfo, QReiseInfo, String> {

    List<ReiseInfo> findBySaison(final String saison);

    /**
     * Customize the {@link QuerydslBindings} for the given root.
     *
     * @param bindings the {@link QuerydslBindings} to customize, will never be {@literal null}.
     * @param qReiseInfo the entity root, will never be {@literal null}.
     */
    @Override
    default void customize(QuerydslBindings bindings, @Nonnull QReiseInfo qReiseInfo) {
        bindings.bind(String.class).first(new SingleValueBinding<StringPath, String>() {
            @Override
            public @Nonnull Predicate bind(@Nonnull StringPath stringPath, @Nonnull String str) {
                return stringPath.containsIgnoreCase(str);
            }
        });
    }
}
