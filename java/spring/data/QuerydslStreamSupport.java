package spring.data;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.data.repository.NoRepositoryBean;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@NoRepositoryBean
public interface QuerydslStreamSupport<T, Q extends EntityPathBase<T>, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T>, QuerydslPredicateExecutor<T>, QuerydslBinderCustomizer<Q> {

    default Stream<T> streamAll(Predicate predicate, OrderSpecifier<?>... orders) {
        return null == orders || 0 == orders.length ? streamAll(predicate) : streamAllHelper(predicate, orders);
    }
    
    default Stream<T> streamAll(Predicate predicate) {
        return streamAll(predicate, false);
    }

    default Stream<T> streamAll(Predicate predicate, boolean parallel) {
        return StreamSupport.stream(findAll(predicate).spliterator(), parallel);
    }
   
    default Stream<T> streamAllHelper(Predicate predicate, OrderSpecifier<?>... orders) {
        return streamAll(predicate, orders, false);
    }

    default Stream<T> streamAll(Predicate predicate, OrderSpecifier<?>[] orders, boolean parallel) {
        return StreamSupport.stream(findAll(predicate, orders).spliterator(), parallel);
    }

    /**
     * Customize the {@link QuerydslBindings} for the given root.
     *
     * @param bindings the {@link QuerydslBindings} to customize, will never be {@literal null}.
     * @param q the entity root, will never be {@literal null}.
     */
    @Override
    default void customize(QuerydslBindings bindings, @Nonnull Q q) {
        bindings.bind(String.class).first(new SingleValueBinding<StringPath, String>() {
            @Override
            public @Nonnull Predicate bind(@Nonnull StringPath stringPath, @Nonnull String str) {
                return stringPath.containsIgnoreCase(str);
            }
        });
    }
}
