# Java quick start

- [Install IntelliJ](#install-intellij)
- [Install InnerBuilder Plugin](#install-innerbuilder-plugin)
- [set @formatter:off](#set-formatteroff)
- [set Hard wrap at 120](#set-hard-wrap-at-120)
- [How to define project specific JDK for Maven:](#how-to-define-project-specific-jdk-for-maven)
- [How to define project specific JDK for Gradle:](#how-to-define-project-specific-jdk-for-gradle)
- [Enable Annotation Processing](#enable-annotation-processing)
- [Choose between JUnit/Gradle Test Runner](#choose-between-junitgradle-test-runner)
- [Where is current java pointing to?](#where-is-current-java-pointing-to)
- [Code Genration](#code-genration)
- [Adding Integration Tests to Gradle](#adding-integration-tests-to-gradle)
- [Difference between <? super T> and <? extends T> in Java](#difference-between-super-t-and-extends-t-in-java)
- [is java classpath safe?](#is-java-classpath-safe)
- [Migrating to Java9 and beyond](#migrating-to-java9-and-beyond)
- [jlink: The Java Linker](#jlink-the-java-linker)
- [Testing](#testing)
- [NoSQL](#nosql)
- [Lazy loading](#lazy-loading)
- [Design Patterns](#design-patterns)
- [To research:](#to-research)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


### Install IntelliJ

### Install InnerBuilder Plugin
https://plugins.jetbrains.com/plugin/7354-innerbuilder


### set @formatter:off
File | Settings | Project Settings | Code Style - General | Formatter Control

### set Hard wrap at 120
File | Settings | Project Settings | Code Style

The reason for it is because GitLab accepts 104 charachers in one line.

### How to define project specific JDK for Maven:
https://stackoverflow.com/questions/19654557/how-to-set-specific-java-version-to-maven

I just recently, after seven long years with Maven, learned about toolchains.xml. Maven has it even documented and supports it from 2.0.9 - toolchains documentation

So I added a toolchain.xml file to my `~/.m2/` folder with following content:

### How to define project specific JDK for Gradle:
https://gitlab.com/lukaszochmanski/blog/blob/master/linux/red-hat/quickstart.md#change-jdk-used-by-gradle

IntelliJ -> Settings -> Build, Execution, Deployment -> Build Tools -> Gradle -> Runner

### Enable Annotation Processing

### Choose between JUnit/Gradle Test Runner
In case the tests are ignored/skipped:
```bash
Execution failed for task ':test'.
> No tests found for given includes:
```
you may also try:
```bash
gradle test -i'
gradle check
```

https://stackoverflow.com/questions/49540962/mapstruct-annotation-processor-does-not-seem-to-work-in-intellij-with-gradle-pro

In Settings -> Build, Execute, Deployment -> Build Tools -> Gradle -> Runner -> Delegate IDE build/run actions to Gradle

Run tests using:

Gradle Test Runner:

```xml
<toolchains xmlns="http://maven.apache.org/TOOLCHAINS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://maven.apache.org/TOOLCHAINS/1.1.0 http://maven.apache.org/xsd/toolchains-1.1.0.xsd">
 <!-- JDK toolchains -->
 <toolchain>
   <type>jdk</type>
   <provides>
     <version>1.8</version>
     <vendor>Oracle Corporation</vendor>
   </provides>
   <configuration>
     <jdkHome>/usr/lib/jvm/java-1.8.0-openjdk</jdkHome>
   </configuration>
 </toolchain>
  <toolchain>
   <type>jdk</type>
   <provides>
     <version>11</version>
     <vendor>Oracle Corporation</vendor>
   </provides>
   <configuration>
     <jdkHome>/usr/lib/jvm/jdk-11.0.1</jdkHome>
   </configuration>
 </toolchain>
  <toolchain>
   <type>jdk</type>
   <provides>
     <version>12</version>
     <vendor>Oracle Corporation</vendor>
   </provides>
   <configuration>
     <jdkHome>/usr/lib/jvm/jdk-12</jdkHome>
   </configuration>
 </toolchain>
  <toolchain>
   <type>jdk</type>
   <provides>
     <version>13</version>
     <vendor>Oracle Corporation</vendor>
   </provides>
   <configuration>
     <jdkHome>/usr/lib/jvm/jdk-13</jdkHome>
   </configuration>
 </toolchain>
</toolchains>
```

###### summary:
```bash
/usr/lib/jvm/java-1.8.0-openjdk
/usr/lib/jvm/jdk-11.0.1
/usr/lib/jvm/jdk-12
/usr/lib/jvm/jdk-13
/opt/idea/idea-IU-191.6183.87/jre64
/usr/libexec/java_home -V
```

### Where is current java pointing to?
https://gitlab.com/lukaszochmanski/blog/blob/master/linux/log.md#where-is-current-java-pointing-to


It allows you to define what different JDKs Maven can use to build the project irrespective of the JDK Maven runs with. Sort of like when you define JDK on project level in IDE.

### Code Genration
* Java Code Generation  
[https://www.baeldung.com/java-annotation-processing-builder](https://www.baeldung.com/java-annotation-processing-builder)  
[https://www.adrianbartnik.de/blog/annotation-processing](https://www.adrianbartnik.de/blog/annotation-processing)

* Use Gradle Task  
how to create a gradle task that will generate the names of fields from @Service
that will be used to write UntiTests

19.10.1. Task inputs and outputs
https://docs.gradle.org/4.0/userguide/more_about_tasks.html#sec:up_to_date_checks

* AutoValue:  
https://stackoverflow.com/questions/21741166/lombok-alternatives-for-clear-code-without-getters-setters-tostring-constructors

* Immutables:
 
### Adding Integration Tests to Gradle
* adding configuration to Gradle
* adding new task to gradle
https://spin.atomicobject.com/2018/07/18/gradle-integration-tests/

### Difference between <? super T> and <? extends T> in Java
https://stackoverflow.com/questions/4343202/difference-between-super-t-and-extends-t-in-java

the following doesn't compile:
```java
        List<Number> foo1 = new ArrayList<Integer>();
        Object x = new Integer(9);
        foo1add(x);
```
the following doesn't compile:
```java
        List<? extends Number> foo2 = new ArrayList<Integer>();
        Object x = new Integer(9);
        foo2.add(x);
```
the following doesn't compile:
```java
List<? extends Number> foo3 = new ArrayList<Number>();
        Number x = new Integer(9);
        foo3.add(x);
```
the following doesn't compile:
```java
List<? extends Number> foo4 = new ArrayList<Number>();
        Number x = new Integer(9);
        foo4.add(x);
```
the following doesn't compile:
```java
List<? extends Object> foo5 = new ArrayList<>();
        Number x = new Integer(9);
        foo5.add(x);
```
the following doesn't compile:
```java
List<?> foo6 = new ArrayList<>();
        Number x = new Integer(9);
        foo6.add(x);
```
this compiles, but it is too broad:
```java
List<Object> foo7 = new ArrayList<>();
        Number x = new Integer(9);
        foo7.add(x);
```
this compiles and it is very correct:
```java
List<? super Number> foo8 = new ArrayList<Number>();
        Number x = new Integer(9);
        foo8.add(x);
```

### is java classpath safe?
java kjlkjl.jar com.studiosus.Main
the above line executes fine and it works without any errors! It means that the classpath is read by classloader on demand. The jars are loaded lazily. They may blow up in runtime.

### Migrating to Java9 and beyond

https://www.baeldung.com/java-9-modularity

https://techbeacon.com/app-dev-testing/legacy-developers-guide-java-9

http://openjdk.java.net/projects/jigsaw/spec/sotms/

https://www.dariawan.com/tutorials/java/using-jaxb-java-11/

### jlink: The Java Linker

http://openjdk.java.net/jeps/282

### Split package
Making JSR 305 Work On Java 9
Error:java: the unnamed module reads package org.w3._1999.xhtml from both commons.xml.reise and commons.xml.converter
Error:java: the unnamed module reads package com.studiosus.xml.stml from both commons.xml.reise and commons.xml.converter
Error:java: the unnamed module reads package com.sun.xml.bind from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.util from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.api from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.unmarshaller from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.marshaller from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2 from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2.util from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2.model.annotation from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2.model.impl from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2.runtime from both jaxb.impl and jaxb.core
Error:java: the unnamed module reads package com.sun.xml.bind.v2.runtime.unmarshaller from both jaxb.impl and jaxb.core

https://blog.codefx.org/java/java-9-migration-guide/#Split-Packages
https://blog.codefx.org/java/jsr-305-java-9/

### Testing
* Mockito

### NoSQL

### Lazy loading
as described here:  
https://stackoverflow.com/questions/29132884/lazy-field-initialization-with-lambdas

you can laz load any field in Java:
```java
static <Z> Supplier<Z> lazily(Supplier<Z> supplier) {
    return new Supplier<Z>() {
        Z value; // = null
        @Override public Z get() {
            if (value == null)
                value = supplier.get();
            return value;
        }
    };
}
```

```java
Supplier<Baz> fieldBaz = lazily(() -> expensiveInitBaz());
Supplier<Goo> fieldGoo = lazily(() -> expensiveInitGoo());
Supplier<Eep> fieldEep = lazily(() -> expensiveInitEep());
```

### Design Patterns
#### Proxy
Good reasons to use proxy:
- you cannot use eager initialization. It is impossible to resolve cycles in graph of dependency eagerily. Proxy and lazy initialization is the solution.
- you don't have access to the resource/service/microservice/network/database at compile time,
- you want to load Java object lazy, because it is slow or you have no enough memory,
- you want to load Java object dynamically on demand, when it is part of the functionality, that the user/client provides the Java Object implementation at runtime
- you have no access to the resource, for example because of blacklisted IP,
- you want to sniff traffic, gather statistical data,
- you want to restrict access to the service/microservice/resource,
- the module is written in another technology and the source code is not available,
- 

### To research:
* Hadoop
* ElasticSearch
* Kafka
