# Generate Optional getter

```
#if($field.modifierStatic)
static ##
#end
Optional<$field.type> ##
#set($name = $StringUtil.capitalizeWithJavaBeanConvention(
$StringUtil.sanitizeJavaIdentifier(
$helper.getPropertyName($field, $project))))
#if ($field.boolean && $field.primitive)
  is##
#else
  get##
#end
${name}() {
  return Optional.ofNullable($field.name);
}
```

https://bohutskyi.com/getter-template-with-optional-in-idea.html
