# Monotlith
  
**Author:** Lukasz Ochmanski  
**Last edited:** 22.08.2022  
**Estimated reading time:** 15 minutes  

[TOC]

## 1. What is a Monolith?

A Monolith is a legacy piece of software that is large and hard to maintain. Very often Monoliths are a synonyms of single-process legacy software. While it is possible to make multi-process monoliths, it is hard to achieve, and usually it is not done. There were many successful monotlihts in past that were performant and well designed, but with an advent of _Cloud Computing_ and _Contenerization_ it would be hard to justify such a choice of architecture, in recent years. I will explain why in the next sections.

### 1.1. How to identify a Monolith?

One may answer the following questions. If one of the answers is yes, it is a strong indicator that you are dealing with a _monolith_.
1. Is the current number of commiters of any repository more than 10? (not total, but currently working on it)
2. Do you often get _merge conflicts_?
3. Do you have a strong need to do _integration testing_ / over _unit testing_?
4. Is your software _Platform_/_Cloud Vendor_ dependent?
5. Is it hard to run your project on the laptop?
6. Does it take longer than 60 seconds to start the application?
7. Is part of the business logic contained inside of _CI/CD_ pipelines?
8. Do DevOps need to modify the any part of the source code in order to make it work on the infrastructure?
9. Is the project hard to profile, optimize and monitor?
10. Is the performance slow when the load is large?
11. Is the application getting out-of-memory error when there are too many requests?
12. Are you using _git branching_ heavily on `remote` to solve your daily problem?
13. Do you have fear of refactoring the code?
13. Is your code entangled, and services contain many dependencies?

## 2. What is a Monorepo?
A Monorepo is the same as Monorepository. Monorepository is a collection of various software projects placed under the same git repository URL.

### 1.1. How to identify a Monorepo?

One may answer the following questions. If one of the answers is yes, it is a strong indicator that you are dealing with a _monorepo_.
1. Does your back-end consist of a single git repository?
2. Are you using _git Submodules_?
3. Do you use _GitFlow_?
4. Are you using _git branching_ heavily on `remote`?
5. Does the remote have many protected branches?
6. Does the remote have branches (other than _main_) that you cannot merge with each other because the history has diverged?
7. Does the remote have branches (other than _main_) that you cannot delete because they contain unique information?
8. Are you releasing software from _branches_, rather than from _git tags_ and _semantic versioning_?
9. Does your application contain no private librarties, that are declared into your `build.gradle` file?
10. Do you have 0 private libraries in your binary repository (Nexus, Artifactory)?
11. Do you store all your dependencies as code, rather than store the dependencies as binaries?
12. Is the number of commiters of the repository more than 10? (not total from the past, but currently working on the repository) 


## 3. Monorepo vs Monolith
The difference is subtle. The _Monolith_ can consist of many git repositories (so it is not a _Monorepo_), and can still be a _Monolith_. So one is not interchangeble with the other. Usually, _Monolith_ are also _Monorepos_, but in rare cases you can see a very well organized _Monolith_ which is well maintained, the code is split into modules or even small independent libraries, which are stored in multiple git repositories, but then it is packed into a single jar, or a single docker image, and later it is started as a single OS process. This would be a perfect example of a _Monolith_ that is not a _Monorepo_. This case is rare because it is just one step away from converting it into microservice infrastructure. It would be quite easy to do, and I don't see why someone would want leavit it like this, but yes, it is theoreticaly possible. If you don't understand the difference yet, don't worry. It is not so important. In 99% cases the _Monolith_ is a _Monorepo_ and vice-versa.
  
**Summary:**
* Monorepo is a git repository that contains all files.
* Monolith is a large software project that runs as a single OS process.

## 4. Semantic versioning
**Introduction**
Consider a version format of `X.Y.Z` (Major.Minor.Patch).  
Bug fixes not affecting the API increment the patch version, backwards compatible API additions/changes increment the minor version, and backwards incompatible API changes increment the major version.  
This also implies, that in a Java eosystem, with microservices communicating with REST,   
there will be a git tag major version matching with a REST URL prefix. For example:  
* git tag 1.0.0 == http://localhost:80/v1/users
* git tag 2.0.0 == http://localhost:80/v2/users

We call this system “Semantic Versioning.” Under this scheme, version numbers and the way they change convey meaning about the underlying code and what has been modified from one version to the next.  

**Summary**  
  
Given a version number `MAJOR.MINOR.PATCH`, increment the:  
  
**MAJOR** version when you make incompatible API changes  
**MINOR** version when you add functionality in a backwards compatible manner  
**PATCH** version when you make backwards compatible bug fixes  
Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.  
  
Source: https://semver.org  

## 5. 10 reasons why using branching is bad

