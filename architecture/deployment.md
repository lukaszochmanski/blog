# Release / Deployment

**Author:** Lukasz Ochmanski  
**Last edited:** Thu 22.08.2022  
**Estimated reading time:** 10 minutes  
  
### 1. Infrastructure as Code
The collection of microservices compose a unit. This unit is our application.
Our application consist of the namespace settings and a graph of dependency between the microservices.
Our application is our infrastructure. Our infrastructure is our application. Our infrastructure is code.

This code must be properly stored in git, just like any other software. Our infrastructure must be stored in git. Any change to the instrastructure must be done in git. This git repository should only contain knowledge about the following two physical infrastructure parts:
* Docker repository and its content
* AWS VM instances

In other words, this git repository contains mapping between docker containers and the machines on which it runs.  
Specifically, this is one-to-many mapping because there could be a single image running many times in given environment, with different configuration.  
For example:  
* image A:1.0.0 will run under address localhost:8080
* image A:1.0.0 will run under address localhost:8081

in this case clearly the configuration (applicaiton.yaml/application.properties) will be different because the port 8081 must be specified.

```mermaid
flowchart LR
    Nexus[(Docker repository)]
    Nexus -->|deploy| AWS1[fa:fa-cog AWS]
```
_Figure 1: Deployment of a binary_

<br/>  
<br/>  

### 2. Current problem description:
All three phases are mixed together. 1. Development, 2. Release, 3. Deployment. This makes the process unnecessarily complex and error-prone.  
There is no clean microservice architecture. There is a mix of monolith and mini-service architecture. Each microservice is deployed manually with a separate button in Gitlab, not through a git trigger of a kubernetes config repo. Microservices should be deployed when a change in kustomization.yaml file is detected, not by a button in GitLab. Each deployment should be versioned in git, and they are not, which makes the checkout of the application from a specific date in the past, not possible. Which means that the global state of the entire system is not reproducible. The environments are based on git branches instead of git tag versions, and there is no additional git repository to keep track of graph of dependency between the specific versions of microservices.
The stability of the system is based on pure luck that the latest versions of all microservices are compatible. If one microservice malfunctions and the bug is detected days later after the deployment, and other deployments are made in the meantime, rollback of this microservice is not possible, because it depends on other microservices. Our infrastructure is stateful and it should be stateless. This can be easily done by storing the entire infrastructure as code in the git repository. We do not have a history of the deployments, and we don’t know which set of versions were stable in the past. We are missing this information. This information is lost with each new deployment. What happens is that exactly the same situation as using maven snapshots instead of maven releases in pom.xml. The state of the application is never reproducible. One day it may work fine, another day it may not work, and it is something that should be fixed.


**SOLUTION:**
- de-couple release from deployment
- do not use snapshots, use final releases with strict versioning scheme from Git tags only
- if something was not tagged in git, cannot be deployed,
- allow releases only in the production environment, do not allow snapshots
- keep track of deployments in a separate git repository
- keep track of all POD settings/application properties
- keep track of all namespace settings
- do not change anything in OpenShift, all environment changes must be done in the git repository only
- do not write dedicated software that depends on the environment. Software must be fully portable and environment agnostic
<br/>  
<br/>  

### 3. Development (Software production)

Before software is released, it must be produced. The development phase is a very broad topic and it will not be described in this article.  
Please assume that the source code is already written, tested and available in a git repository.  
Continue reading the next section.  

### 4. Release (Software distribution)

Release, sometimes called generally as "build", is the process of placing/publishing a binary into the repository.  
Later this software could be distributed to the end-clients, by providing access to the repository.  
The distributed binary usually does **NOT** contain the source code.  
It means that the client will not be able to determine what the binary contains, who is the author, or where the binary comes from.   
Binaries can have different formats:  
they could be Docker images, C libraries, Windows executables, Linux executables, Java Bytecode files, Java *.jar files, etc.  
The most popular format in which software is distributed in recent years is the Docker file.  
It is very portable and reliable. Most cloud providers use it as a standard.  

> source code ---> binary ----> Artifactory/ECR/Nexus/JFrog

example:
> Java ----> Docker image ----> Nexus
<br/>

```mermaid
flowchart LR
subgraph Nexus
    direction TB
    subgraph docker-releases
        direction BT
        R1[A:2.0.0]
        R2[B:1.2.7]
        R3[C:1.3.9]
    end
end
    AA[Team A] -->|checkout| BA(Source Code A) -->|commit| BA(Source Code A)
    AB[Team B] -->|checkout| BB(Source Code B) -->|commit| BB(Source Code B)
    AC[Team C] -->|checkout| BC(Source Code C) -->|commit| BC(Source Code C)
    BA -->|push| C{GitLab}
    BB -->|push| C{GitLab}
    BC -->|push| C{GitLab}
    C -->|docker build| D1[fa:fa-car Docker Image A:2.0.0]
    C -->|docker build| D2[fa:fa-car Docker Image B:1.2.7]
    C -->|docker build| D3[fa:fa-car Docker Image C:1.3.9]
    D1 -->|docker push| Nexus
    D2 -->|docker push| Nexus
    D3 -->|docker push| Nexus
```
_Figure 2: Release_
<br/>  
<br/>  


### 5. Deployment (Software intallation)
Before the era of cloud computing and Docker images, this step was done on premises with simple CLI tools.  
The copy of the executable file was obtained with FTP protocol and started manually on the machine.  
Today we have CD pipelines which automate this process, but the idea is still the same.  
The deployment is the process of copying an executable file from the repository and running it.  
This process is fairly simple and can be done by anyone with basic administrative skills.  
The process of deployment also known in the old day as installation/upgrade can be done by an operator that is not a programmer.  
The operator is different from a developer. The operator doesn't need to know anything about the process  
of producing or distributing software.  
  
In order to perform a deployment we need the following parameters:
* coordinates (respository URL, releases/snapshots, group ID, artifact ID)
* version (example: 1.0.0)
* configuration (example: database credentials, port number, DNS entry)

```mermaid
flowchart LR
subgraph Nexus
    direction TB
    subgraph docker-releases
        direction BT
        R1[A:2.0.0]
        R2[B:1.2.7]
        R3[C:1.3.9]
    end
end
    Nexus -->|docker run A:2.0.2| AWS1[fa:fa-cog Dev]
    Nexus -->|docker run B:1.2.8| AWS1[fa:fa-cog Dev]
    Nexus -->|docker run C:1.3.11| AWS1[fa:fa-cog Dev]

    Nexus -->|docker run A:2.0.1| AWS2[fa:fa-cog Test]
    Nexus -->|docker run B:1.2.7| AWS2[fa:fa-cog Test]
    Nexus -->|docker run C:1.3.10| AWS2[fa:fa-cog Test]
    
    Nexus -->|docker run A:2.0.0| AWS3[fa:fa-cog Prod]
    Nexus -->|docker run B:1.2.6| AWS3[fa:fa-cog Prod]
    Nexus -->|docker run C:1.3.9| AWS3[fa:fa-cog Prod]
```
_Figure 3: Deployment_

<br/>  
<br/>  
Example deployment scripts:  

### localhost
1. docker run nexus.bludid.net/docker-releases/de/blueid/service-a:2.0.4
2. docker run nexus.bludid.net/docker-releases/de/blueid/service-b:1.2.9
3. docker run nexus.bludid.net/docker-releases/de/blueid/service-c:1.3.12
4. docker run mysql

### dev
1. docker run nexus.bludid.net/docker-releases/de/blueid/service-a:2.0.3
2. docker run nexus.bludid.net/docker-releases/de/blueid/service-b:1.2.8
3. docker run nexus.bludid.net/docker-releases/de/blueid/service-c:1.3.11
4. docker run mysql

### test
1. docker run nexus.bludid.net/docker-releases/de/blueid/service-a:2.0.2
2. docker run nexus.bludid.net/docker-releases/de/blueid/service-b:1.2.7
3. docker run nexus.bludid.net/docker-releases/de/blueid/service-c:1.3.10
4. docker run mysql

### prod
1. docker run nexus.bludid.net/docker-releases/de/blueid/service-a:2.0.1
2. docker run nexus.bludid.net/docker-releases/de/blueid/service-b:1.2.6
3. docker run nexus.bludid.net/docker-releases/de/blueid/service-c:1.3.9
4. docker run mysql
<br/>  
<br/>  

### 6. Complete Software release/deployment cycle
Complete Software release/deployment cycle consists of three basic stages:
1. Software production (development)
2. Software distribution (release)
3. Software installation (deployment)  

stages 2 and 3 are shown below on Figure 4:  

```mermaid
flowchart LR
    GitLab -->|docker build| Nexus[(Docker repository)]

    Nexus -->|docker run| AWS1[fa:fa-cog Dev]
    Nexus -->|docker run| AWS2[fa:fa-cog Test]
    Nexus -->|docker run| AWS3[fa:fa-cog Prod]
```
_Figure 4: Complete Software release/deployment cycle_

<br/>  
<br/>  


####  7. Summary
```mermaid
flowchart LR
    GitLab -->|release| Nexus[(Docker repository)]

    Nexus -->|deploy| AWS1[fa:fa-cog AWS]
```
_Figure 5: Summary_

<br/>  
<br/>  
