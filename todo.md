# read this:
The Initiative for Open Authentication (OATH)

https://openauthentication.org

https://www.varonis.com/blog/what-is-oauth/

https://www.cubrid.org/blog/dancing-with-oauth-understanding-how-authorization-works

https://jwt.io/

### REST vs GraphQL
https://blog.goodapi.co/rest-vs-graphql-a-critical-review-5f77392658e7

### Building a Gateway Using Spring MVC
https://cloud.spring.io/spring-cloud-gateway/1.0.x/multi/multi__building_a_gateway_using_spring_mvc.html

# my todo list:
* create styles with html as here: https://developer.github.com/v3/
* intall teampasswordmanager
