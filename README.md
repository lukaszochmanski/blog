# Lukasz Ochmanski

### Best Practices
Personal blog about software development<br/>
Author: Lukasz Ochmanski (gitlab@ochmanski.de)<br/>
Last edited: 2022-08-16 11:28 CET<br/>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Prerequisites](#prerequisites)
- [Version control system (VCS)](#version-control-system-vcs)
        - [Notes:](#notes)
- [Best practices: Branches, commits, merging](#best-practices-branches-commits-merging)
        - [Note:](#note)
    - [How to switch between branches?](#how-to-switch-between-branches)
    - [How to switch between branches if there are unstashed changes?](#how-to-switch-between-branches-if-there-are-unstashed-changes)
- [Code quality](#code-quality)
- [Test Driven Development (TDD)](#test-driven-development-tdd)
- [Clean Code](#clean-code)
        - [Notes:](#notes-1)
- [Stay DRY](#stay-dry)
- [SOLID](#solid)
- [The Law of Demeter (LoD)](#the-law-of-demeter-lod)
- [Avoid abusing comments](#avoid-abusing-comments)
    - [Examples for what comments are NOT made for:](#examples-for-what-comments-are-not-made-for)
    - [Examples of GOOD usage of comments:](#examples-of-good-usage-of-comments)
        - [Do's and dont's:](#dos-and-donts)
        - [Always remember:](#always-remember)
- [Java specific hints](#java-specific-hints)
    - [Why "switch" and "if" statements are bad](#why-switch-and-if-statements-are-bad)
- [Singleton](#singleton)
    - [Conclusions and usage rules](#conclusions-and-usage-rules)
    - [6 ways to create Singleton objects:](#6-ways-to-create-singleton-objects)
    - [Making Singletons With Enum in Java](#making-singletons-with-enum-in-java)
- [Stateful vs stateless](#stateful-vs-stateless)
- [Rich vs Anemic Domain Model](#rich-vs-anemic-domain-model)
- [More rules](#more-rules)
- [Virtualization](#virtualization)
- [JVM](#jvm)
- [Docker](#docker)
- [VM](#vm)
- [Microservices](#microservices)
- [Reading:](#reading)
- [Topics for discussion:](#topics-for-discussion)
- [The Requirements:](#the-requirements)
- [How to achieve this:](#how-to-achieve-this)
- [Conclusions: (rethink...)](#conclusions-rethink)
- [Versioning](#versioning)
- [Dependencies topology](#dependencies-topology)
- [Kafka](#kafka)
- [Rollbacks](#rollbacks)
- [Kubernetes](#kubernetes)
- [Minikube](#minikube)
- [Load balancers](#load-balancers)
- [Git](#git)
- [Ignore OS specific files](#ignore-os-specific-files)
- [Branching workflows](#branching-workflows)
- [example script that creates a sequence of versions](#example-script-that-creates-a-sequence-of-versions)
- [Undo a commit, making it a topic branch](#undo-a-commit-making-it-a-topic-branch)
- [How to fix a commit](#how-to-fix-a-commit)
- [Spring](#spring)
- [application.properties](#applicationproperties)
- [Spring Boot](#spring-boot)
    - [Using QueryDSL annotation processor with Gradle and IntelliJ IDEA](#using-querydsl-annotation-processor-with-gradle-and-intellij-idea)
    - [90. Hot Swapping](#90-hot-swapping)
    - [91. Build](#91-build)
- [Software Architecture](#software-architecture)
    - [5 reasons why you should use field-based access with JPA and Hibernate](#5-reasons-why-you-should-use-field-based-access-with-jpa-and-hibernate)
    - [REST vs GraphQL](#rest-vs-graphql)
    - [Building a Gateway Using Spring MVC](#building-a-gateway-using-spring-mvc)
    - [Linux shell](#linux-shell)
- [read this:](#read-this)
- [my todo list:](#my-todo-list)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About

In this article we present a few rules and tips to be followed when writing Java code. This is to ensure that the project is clean, readable and maintainable.

## Prerequisites

Before you start, make sure that you are familiar with the following topics:

* Learn fundamentals of functional programming paradigm.
* Learn everything about Java Streams, Optionals and Lambda Expressions.
* Learn how to use method references, as well as: `Function<T, R>`, `BiFunction<T, U, R>`, `Consumer<T>`, `BiConsumer<T, U>`, `Supplier<T>`.
* Read about default and static methods in interfaces.
* Learn how multiple inheritance in Java works.

## Version control system (VCS)

Make yourself familiar with git and the respective processes:

* Learn how to unleash the full git potential. [https://learngitbranching.js.org/](https://learngitbranching.js.org/).
* What is an Atomic commit? Check it out here: [https://www.freshconsulting.com/atomic-commits/](https://www.freshconsulting.com/atomic-commits/).
* Watch also this video on git: [Tech Talk: Linus Torvalds on git](https://www.youtube.com/watch?v=4XpnKHJAok8&t=2539s).

#### Notes: 
1. It doesn't matter how your git commits look like when they are first 'born'. You can fix them up any time e.g. with the command `git rebase -i until they are merged into develop. **Important:** git rebase is only permitted if you haven't pushed your changes anywhere else. More about rewriting history [here](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History).
2. Commit early, commit often. Don't wait until a block of work is done. When you are done, fix up your history, and make it look like you had perfect foresight, and the most beautiful code just flew out of your keyboard on first try. 
3. The above is not for vanity, but to make the code reviewers' and maintainers' lifes easier!
4. Merge early, merge often.
> I do several merges per day!
- Linus Torvalds, see here: [Tech Talk: Linus Torvalds on git https://www.youtube.com/watch?v=4XpnKHJAok8&t=2539s](https://www.youtube.com/watch?v=4XpnKHJAok8&t=2539s))

## Best practices: Branches, commits, merging

Do several commits per branch. If Linus Torvald says that you should merge often (see notes above), this implies that you should branch even more often. The more branches you make, the easier is to manage smaller units of work. This means that a branch is not only used for a task. Branches should be backups to which you can return whenever you feel. You can later decide if you discard or use this branch. Making many small branches divides your work into smaller units. Therefore, it helps to manage your work better. It allows you to avoid cherry-picking and resolving conflicts after. Ideally, you should divide your work early and then put all the pieces together later. Those pieces will be testable independently. This will result in a cleaner design of your classes.

Create commits when you refactor your code. In case something does not work you can easily bisect in O(log). This technique allows you to make fast and agressive code changes without any risk. Creating more commits does not take more time. Write your code without fear that something can go wrong. Don't hesitate to make mistakes! Do not try to get it right at the first shot. You may progress as fast as possible. Your code will not be perfect anyway. You can easily fix it at any time. 

**Refactor, refactor, refactor** should actually say: **refactor, commit, refactor, commit, refactor, commit**. Git helps you to write code fast and spot bugs later. Git helps you to become a better programmer.  

#### Note: 
Please do not mix up **refactoring** with **implementing some feature / code change / optimization / etc.** Otherwise, you will not be able to separate those things in code reviews. A reviewer will easily lose focus. Create separate commits for refactoring. Perhaps, group those commits in branches!

As mentioned at the beginning, make commits as small as possible, as long as they are atomic. Read what an atomic commit is.

### How to switch between branches?
Use the command:
```bash
git checkout team/itk/loc/experimental-branch
```

### How to switch between branches if there are unstashed changes?

1. The easiest way is to use `git stash`:
```bash
git stash
git checkout team/itk/loc/experimental-branch-two
```
There are two problems with stashes:
* once stash is popped you cannot reapply it anywhere else. Use `git stash apply` instead.
* they are not associated with a specific branch. You can push and pop anywhere. You can easily forget where stash belongs to.
To avoid this, you must remember the id or the name of the stash. Giving a name is done as follows:
```bash
git stash push -m stashname
```
You can look up all your saved stashes with the following command:
```bash
git stash list
```
Then execute the `apply` and `drop` commands:
```bash
git stash apply stash@{0}  
git stash drop stash@{0}
```
2. Alternatively, you can use a sequence of `commit`s and `reset`s:
```bash
git add . # use only if you want to stash everything into one commit. Note that if you want to distinguish between changes in index and worktree, and you want to preserve both, you should create two commits (described below)
git commit -m 'this commit will be undone'
git checkout team/itk/loc/experimental-branch-two
```
Now, do your work in this branch and then execute the following:
```bash
git add . # use only if you want to stash everything into one commit. Note that if you want to distinguish between changes in index and worktree, and you want to preserve both, you should create two commits (described below)
git commit -m 'this commit will be undone as well'
git checkout team/itk/loc/experimental-branch-one
git reset HEAD~1 --mixed
```

Thus, you keep working from the same point.

If you want to keep files that are not in index, commit twice: 

1. First, only `git commit` (staged) items in index (let's call it commit ***#abcd***).
2. Second, `git add` and `git commit` the remaining (unstaged) items (let's call it commit ***#cdef***).

pop commit ***#cdef*** with unstaged changes into your working tree:
```bash
git reset --mixed HEAD~1 # (short for: git reset --soft HEAD~1 && git reset --mixed)
```

pop commit ***#abcd*** with staged changes into your index:
```bash
git reset --soft HEAD~1
```
The working tree should be recreated and you should be able to resume your work from that point in time. Alternatively, you can use IntelliJ Smart Checkout feature. You may also create a script or an alias to do all this for you.


## Code quality
In order to keep the project in good shape, always follow these rules:

* Get rid of all compilation warnings.
* Run static analysis of your IDE.
* Use SonarQube Quality Gates.
* Run dynamic analysis (code review).
* Perform dependencies analysis.

Even if you have fixed every compiler error and linker error that your compiler has thrown, you may still receive **warnings**. These warnings won't keep your code from compiling (unless you ask the compiler to treat warnings as errors), and you might be tempted to just ignore them. There are, however, some good reasons not to do so:

The great thing about compiler warnings is that they are often indicators of future bugs that you would otherwise see only at runtime. For instance, if you see your compiler complaining about an **assignment in conditional** this might mean that you've written **Catch Bugs** that are hard to find in testing.

Compilers will always warn you about things that might be difficult to find during testing. For example, your compiler warns that you are using an uninitialized variable. This can be hard to find in testing if you declare your variable far from where you first use it. Using an uninitialized variable in this case simply means getting its value. The solution to the problem is fairly simple: Initializing the variable.

## Test Driven Development (TDD)

Read the following articles:
* https://martinfowler.com/articles/mocksArentStubs.html
* https://blog.cleancoder.com/uncle-bob/2014/05/14/TheLittleMocker.html

For any task, do only TDD and keep code coverage to a maximum possible by following these rules:

* Do not use static methods (impossible to mock without [Javassist](http://jboss-javassist.github.io/javassist/)).
* Do not use constructors (impossible to mock without [Javassist](http://jboss-javassist.github.io/javassist/)).
* Do not make every method public (use package access).
* Do not use PowerMock. For more information why we don't use PowerMock, read this article: [link](https://automationrhapsody.com/powermock-examples-better-not-use/).  
* [Javassist](http://jboss-javassist.github.io/javassist/) (Java Programming Assistant) makes Java bytecode manipulation simple. It is a class library for editing bytecodes in Java; it enables Java programs to define a new class at runtime and to modify a class file when the JVM loads it.

<a name="clean_code"></a>
## Clean Code

For an introduction to Clean Code, read the following book: 

*Clean Code: A Handbook of Agile Software Craftsmanship 1st Edition*
by Robert C. Martin
ISBN-13: 978-0132350884
ISBN-10: 0132350882

#### Notes:

* Do not return **null**. Return an empty Optional or an empty collection when needed.
* Do not pass **null**.
* Do not pass more than two arguments to methods, pass **DTOs**.
* Do not pass **boolean** or **null** arguments, pass **lambda** or **method reference**.
* Wrap all 3rd party libraries with your methods (create an extra layer), so you can easily replace the libraries later.

The most basic, yet most important rule is to divide everything into tiny pieces. **Small is good. Large is bad!** To illustrate this, ask yourself the following questions:

* Would you rather have one hundred small drawers in your workshop or one large bag with all the tools? which one would you choose?
* Would you prefer one hundred small classes grouped into multiple packages or one class with all the methods? which one would you choose?

The conclusion is obvious: Always divide classes and methods into indivisible, atomic, independent parts. Make methods as small as possible, so it is easy to test them. 

If you are not sure, what the smallest unit be, follow these rules:

* One class - one screen
* One method does one thing. If it does two things, they are two methods. 
* Each "thing" that the method (unit) does, must be unit tested in seperate tests.

If you see something wrong - fix it. It is your project. You are responsible to keep it clean!

## Stay DRY

DRY stands for *Don’t Repeat Yourself*, one of the most important acronyms in coding methodology. If you repeat youself, you introduce complexity to the code making it more difficult to maintain. For instance, if one section of code requires a change and the same code is repeated in another file, you may fail to realize the need for an additional update. When you follow the single responsibility principle, there is no need to repeat code, because your programming is focused and created in a way that encourages reuse, not repetition.

If you see a duplicate - get rid of it.

## SOLID

SOLID is an acronym and stands for the five design principles to make code understandable, flexible and maintainable: 

* **S** ingle Responsibility Principle SRP, a class should have only a single responsibility (i.e. changes to only one part of the software's specification should be able to affect the specification of the class).  
* **O** pen/Closed Principle OCP, "software entities … should be open for extension, but closed for modification."  
* **L** iskov Substitution Principle LSP, "objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program." See also design by contract.  
* **I** nterface Segregation Principle ISP, "many client-specific interfaces are better than one general-purpose interface."  
* **D** ependency Inversion Principle DIP, one should "depend upon abstractions, [not] concretions."  

## The Law of Demeter (LoD)

The Law of Demeter (LoD) or principle of least knowledge is a design guideline for developing software, particularly object-oriented programs. In its general form, the LoD is a specific case of loose coupling. The guideline was proposed by Ian Holland at Northeastern University towards the end of 1987, and can be succinctly summarized in each of the following ways:

* Each unit should have only limited knowledge about other units. Only units closely related to the current unit.
* Each unit should only talk to its friends. Don't talk to strangers. Only talk to your immediate friends.

## Avoid abusing comments

Comments aren’t to be treated lightly. When commenting on code, the current functionality is explained in terms of variables and results. 

### Examples for what comments are NOT made for:  

* Describing the syntax of the code:

```java
 /**
 * Constructor.
 */
```

```java
/* 
 * =============================  
 *         Inner class  
 * =============================  
 */  
```
    
* Writing explanatory notes to yourself:

```java
/* Will finish this later… */
```

* Blaming stuff on other people:

```java
/* John coded this. Ask him. */
```

* Writing vague statements:

```java
/* This is another math function. */
```

* Erasing chunks of code. Sometimes people are not sure of erasing things and it’s not absolutely evil to comment that code instead. But you should always remove it afterwards. Otherwise it will be terribly confusing. If the code is documented via embedded comments, the team members have to make sure they are there for a reason.

### Examples of GOOD usage of comments:

* Editor fold:

```java
// <editor-fold defaultstate="collapsed" desc="user-description">
  ...any code...
// </editor-fold>
```

* Authoring specifications:

```java
/* Coded by John, November 13th 2010 */
```

* Detailed statements on the functionality of a method or procedure:
 
```java
/* This function validates the login form with the aid of the e-mail check function */
```

* Quick notifications or labels that state where a recent change was made:

```java
/* Added e-mail validation procedure */
```

#### Do's and dont's:

* Do not use **@Todo** annotations in code that is committed to the GIT server, for example: from time to time our code is tracked by some external audit agencies to evaluate the code quality. Each **@Todo** annotation reduces the quality factor of our software because this means: Something has to be done but no one cares about it.
* High level programming languages were created to declare things in a human friendly way. Each comment means that you are unable to express your intentions in the language you use. e.g. Java. This is an evidence that you as a programmer or the programming language failed.

An example from **build.xml**:

```ant
<property name="jdk.home" location="${env.JAVA_HOME}" />  <!-- if JAVA_HOME is already defined in build.sh, this line has no effect -->
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It is a failure of the Ant language, bacause you cannot name this action/method/function. There is no other way of expressing the intentions of the code.  

* If you comment a block of code, this means it is a method. Please do not hesitate to change it with a proper shortcut in your IDE i.e. generate a method. Such simple refactors are harmless to the project.
* Utilize git commit message body and utilize git blame to supplement comments in the source code. Always check for hints in git commit message.

Here's an example:

```git
-        return generator.apply(Math.toIntExact(getExactSizeIfKnown()));
+        long exactSizeIfKnown = getExactSizeIfKnown();
+        return exactSizeIfKnown < 0 ? toList().toArray((B[]) new Object[0]) : generator.apply(Math.toIntExact(exactSizeIfKnown));
```
And here's the commit message for this:

```git
C5-12345 Fixed error in IterableFj

If spliterator().getExactSizeKnown() returns -1, toList().toArray() will be called instead of applying IntFunction<B[]>
```

#### Always remember: 
If you see something wrong - fix it. It is your project. You are responsible to keep it clean!

## Java specific hints

For a comprehensive introduction to effective Java programming, read the following book:

*Effective Java (3rd Edition) 3rd Edition*
by Joshua Bloch
ISBN-13: 978-0134685991
ISBN-10: 0134685997

However, there are more important Java specific topics, which are not covered by "Effective Java" and which you should apply:

* Favor composition over inheritance.
* Avoid casts at all cost. Use generics. Favor question mark `<?>` - called the wildcard - over `<Object>`,
* Avoid using reflection. Favor precompilation.
* Use **Lombok** to generate code.
* Do not use constructors.
* Favor factories and builders.
* Use fluent setters.
* Get rid of **if** statements.
* Get rid of **switch** statements.
* Utilize the power of **enums** (enum example with method references provided below).
* Eliminate **NullPointerExceptions**.
* Never return **null**. Instead return **Optional/List**.
* Use precompilers.
* Use Java Annotation Processor.
* Use **QueryDSL**.
* Use **JPA**.
* Do not write **SQL** as Strings.
* Favor frameworks, e.g. **Spring**, **Quarkus**.
* Use **Dependency Injection**.

### Why "switch" and "if" statements are bad 
1. They are imperative, not declarative.  
2. The logic has to be repeated and cannot be reused, divided or iterated. (You have to copy and paste the whole block)

Following, you find examples of how to get rid of `switch`/`if` statements:

```java
private PersonContact getContact(Adress adress, Person person) {
    switch (adress) {
        case COMMERCIAL : {
            return person.getCommercial();
        }
        case LOGISTIC: {
            return person.getLogistic();
        }
        case HOME: {
            return person.getHome();
        }
    }
    throw new IllegalArgumentException("Not found address type.");
}
```

#### Solution: 

Create an `enum` that represents the same logic:

```java
public enum Address {
    COMMERCIAL(Person::setCommercial, Person::getCommercial),
    LOGISTIC(Person::setLogistic, Person::getLogistic),
    HOME(Person::setHome, Person::getHome);

    private BiConsumer<Person, PersonContact> consumer;
    private Function<Person, PersonContact> function;
}
```

and call one-liner:

```java
private PersonContact getContact(Adress adress, Person person) {
    return adress.function.apply(person);
}
```

The same operation can be done for any piece of code in Java. Which means that no `if` statements are needed. Generally said, the `if` statement is an optional feature of the language and should be avoided. Remember declarative before imperative! A good rule of thumb is the following: 

If your switch has more than two cases, declare it as an inner enum. If your switch has only two cases, use the ternary operator.

## Singleton

You should use factories instead of constructing objects by yourself. Singleton is a factory of a particular object. As for all tools, the basic rule applies: Singleton is good, unless you use it wrong.

|  Singleton: | Instance Object: |
|:-|:-|
| object constructed only once | object constructed every time |
| fast | slow |
| reduceed memory footprint | heavy on memory |
| not unit-testable when created without Dependency Injection | easily unit-testable |
| not extendable, ([link](http://geekswithblogs.net/AngelEyes/archive/2013/09/08/singleton-i-love-you-but-youre-bringing-me-down-re-uploaded.aspx)) | extendable |
| controls concurrent access to a shared resource | not concurrent |
| requires knowledge and self-discipline from programmers - if used incorrectly will cause hard-to-find bugs | easy to implement and reliable |
| single point in your application to control the construction of the object (good design) | object is constructed in multiple places in your application, each instance can be different. Sometimes construction can fail unexpectedly. It is hard to tell when, because the process is context sensitive.|

### Conclusions and usage rules 

Singletons are generaly good, unless they retain state. A good singleton is a singleton where all of the reachable objects are immutable. If all objects are immutable than Singleton has no global state, as everything is constant, e.g. Java enum.  
If it is possible to use a singleton, you should prefer it. The same applies to immutability. If you can do it, you shouldn't think about it twice. Just do it. The benefits of immutable objects and benefits of proper singletons are great.  

However, there are some cases where Singletons with shared global state are also acceptable. Logging is a good example for that. It is loaded with Singletons and global state. It is acceptable because your application does not behave any different whether or not a given logger is enabled. The information flows in one way - from your application into the logger. You should still inject your logger if you want your test it, but in general loggers are not harmful despite being full of state.

Any other use of state Singleton should be avoided. The following rules of thumb give a good orientation:
 
 * If a class has at least one mutable field it should NOT be a Singleton. 
 * If a class has at least one non-final field it should NOT be a Singleton. 
 * Conversely, if a class has no mutable non-final fields it SHOULD be a Singleton. 
 * If a class has only immutable final fields (known as constants) it SHOULD be a Singleton. 
 * If all fields of a class are final and immutable (known as constants) the class SHOULD be a Singleton. 
 * If a class has no fields it SHOULD be a Singleton.

### 6 ways to create Singleton objects:
1. Dependency Injection
2. Reflection
3. Enum
4. Field initialization
5. public getInstance() Private constructor
6. static synchronized block {...}

#### Note:
Always use Dependency Injection to create Singletons.

For further reading, see the following article: [http://geekswithblogs.net/AngelEyes/archive/2013/09/08/singleton-i-love-you-but-youre-bringing-me-down-re-uploaded.aspx](http://geekswithblogs.net/AngelEyes/archive/2013/09/08/singleton-i-love-you-but-youre-bringing-me-down-re-uploaded.aspx)

### Making Singletons With Enum in Java

If you are not familiar with the problem, there exists only one proper way to initialize singleton `lazily`, `fast` and `thread safe`:

```java
public enum Singleton {
    INSTANCE;
}
```

For a comprehensive explanation of the problem, read the following article: [https://dzone.com/articles/java-singletons-using-enum](https://dzone.com/articles/java-singletons-using-enum)

#### Notes:

* Never use `getInstance()`!
* Always use `enum`!

If you ever see `getInstance()` in the project, please change it because it is neither **thread-safe**, **fast** nor **lazy**.

## More rules
* Never use non-final, non-static, mutable fields in singleton classes, which are all our services!
* Use streams and dynamic data structures instead of static constructs. Do not store big objects in memory.
* Do not use synchronization. Do not use locks or locking structures. Use asynchronous approach. 
* When locking/synchronization seems to be the only solution, think again and try to avoid it! Almost everything that can, should be done concurrently. The only reason to not use concurrency or parallelism is if the concurrent solution is slower or not acceptable because of very high memory footprint. 
    * **Fine-grained vs. coarse-grained:** If locking is needed, use it for the smallest possible parts. Never do a synchronizing over a "huge" method that requires a lot of resources.   
    * **Optimistic locking:** If locking is needed, use optimistic locking before pessimistic locking first, and see if it works.
* Always use lazy loading, unless you can't.
* Do not use caching, unless you have to.
* Correctness is more important than speed.
* Premature optimization is wrong. Never optimize first. Always make it work first. Profile later. Index and cache at the end.
* Never favor imperative programming over declarative programming, unless it is slow.
* Using imperative programming, because you don't know how to debug java8, or because you don't understand streams and lambda expression is a poor excuse. You should learn it and never go back to imperative. Since we have Java8, using pre-Java8 is deprecated!
* Use a declarative approach first. Imperative approach is only acceptable in exceptional and well justified cases.

## Rich vs Anemic Domain Model
Once I said: "Please don't mix data object with service objects". I said it because this is an official recommendation of J2EE. Spring Framework and Testing Frameworks usually support Anemic Domain Model very well. It is the only way that Java Beans can be injected using Inversion of Control i.e. Dependency Injection. Now, when you know exactly how Inversion of Control works, you can experiment with Rich Domain Model. In general it is not harmful to divide everything into data and services. It is time to reevaluate our strategy. So the ultimate conclusion is that Entities and DTOs also may contain some business logic, as long as it is simple, clear and will not grow. Let's not go to the extreme and let's not create methods that accept a series of arguments (zero is ideal, one is tolerable), or God forbid methods that accept service objects. This would not be correct. A simple **INSTANCE** method is very welcome. A **STATIC** method is not welcome at all. All **STATIC** methods/functions belong to the service, **INSTANCE** methods belong to the domain:  
> State that reflects the business situation is controlled and used here, even though the technical details of storing it are delegated to the infrastructure. This layer is the heart of business software.     
Eric Evans  
  
Domain can be stateful, and it is stateful. Services cannot be stateful. Singleton Services must be stateless. If you need to access an object instance variable or modify the state of a variable, you are welcome to add some more instance methods to the object.  

## Virtualization
| - | Docker | JVM | VM |
| ------ | ------ | ------ | ------ |
| main memory footprint | normal | very good | very bad |
| file size | large | **very small** | very large |
| CPU usage | good | very good | very bad |
| responsivnes/speed | good | very good | very bad |
| license price | free | free | expensive |
| first set up time/effort | large effort | **no effort** | large effort |
| second set up time/effort | no effort | no effort | large effort |
| hot swap during development | poor | **very good**| not applicable |
| versioning git | **very good**| **very good**| not possible |
| versioning images | **good** | **good** | **good** |
| stateful machine | **yes** | no | **yes** |
| warm start | experimental | no | yes |
| cold start | yes | yes | yes |
| cost of maintenance by developers | no effort | no effort | horrible |
| devops team required | yes | not required | not required |
| cost of maintenance by operations | no effort | large effort | no effort |
| cost of update by developers | no effort | no effort | horrible |
| cost of update by operations | no effort | large effort | no effort |
| speed of deployment on site | **the fastest**| slow | fast |
| portability | good | good | **the best** |
| difficulty for the developers | difficult | easy | easy |
| difficulty for the client | difficult | very difficult | **easy** |
| supported software | any CLI | only one Java process | **any CLI/GUI**|
| supported programming languages | all | limited choice | **all**|

as you can see from the table above, depending on your needs and your resources you can choose suitable virtualization technology.
The first question is: do you need to develop in Java? If no, you can forget about using JVM, if yes, go on and read the text below.
## JVM
#### pros:
- JVM is the best for fast development and works without any overhead
- the only virtualization technology that doesn't store images of binary files, it actually interprets the source (bytecode). So it is extremely efficient, in terms of file size, during development phase.
- provides granular versioning powered by git
- unbeatable for the speed of development and integration with tools (maven, gradle, plugins, IDE)
- build process is strongly developer oriented, you can run the application, debug, test, hot swap without, restart, rebuild or recreating a JAR bundle
- the only virtualization technology that provides hot swap of source code without restart and rebuild of the image
- very compact size of a bundle (no images, no sources, no history, only pure bytecode)

#### cons:
- JVM works only for Java ecosystem
- JVM is not so easy to install, configure, clone and run for clients
- very few databases are running in a JVM (hsqldb, h2, Apache Derby)
- no incremental build: creating a EAR, JAR or WAR requires to precompile, compile, start everything from the beginning, which can take a long time
- cold start problem,
- build process is not customer oriented (a small update requires either: a full recompilation/rebuild, or bringing a new EAR/WAR/JAR)
- the bundle is statless (no live snapshots) so you can't pause, stop, resume
- JVM may be slow to start in production, compared to other Ahead-of-time compilation solutions.
- you can run only one process in a JVM
- JVM is very old (1994) and out-of-date, JVM is missing many feature available in Docker containers
- if you are not using Java, docker container will be a better choice

## Docker 
#### pros:
- reliable solution for operators, devops and end-clients
- new standard for distributing binaries
- supports any programming lanugage and can virtualize any type of software
- the most reliable way of bundling dependencies
- build process is customer oriented: docker image has layers (you don't need to compile and run everything from zero), you can create docker images faster than JARs, by only changing one layer
- can run multiple processes in one docker container
- it is the future of distributing sofware, it is even better than famous Java motto: "Write once, run anywhere" (WORA)
- compatible with many new technologies: Docker Compose, Kubernetes, MiniKube
- very simple, flexible and reliable for maintanance and updates
- provides decent level of abstraction at low cost
- works great with Continous Integration
- compatible with all cloud vendors
- perfect for QA, perfect for IT support and reproducing errors, solving tickets
- perfect for recreating the enviroment 1 to 1 in case of weird, unusal, crashes, errors
- it is possible to rewind to a snapshot, and retry whatever you were doing safely without any side effects (it is an experimantal feture called: checkpoint)
- fast image checkout which provides runtime enviroment quickly and reliably

#### cons:
- works only with CLI
- not suited for developers
- image size grows very quickly
- hot swapping files impossible
- slow re-build
- you end-up with a single shell command in a single docker layer, in order to compress the image size, 
- no support for IDE
- adds another layer of virtualization on top of the existing one
- adds complexity to the project
- runtime overhead
- learning curve for everyone
- not backward compatible with all versions of Java (pre Java 9 don't recognize this type of virtualization, so it crashes)
- docker container and JVM duplicate some functionalities, they serve the same purpose, therefore they contradict each other

## VM
#### pros:
- provides unbeatable level of abstraction and reliability
- works with GUI
- works with all types of software
- provides IDE support (you can virtualize the development enviroment)
- warm start
- has snapshots (pause, stop, resume)
- retains data (fully featured file system)
- compatible wih all cloud vendors
- perfect for QA, perfect for IT support and reproducing errors, solving tickets
- perfect for recreating the enviroment 1 to 1 in case of weird, unusal, crashes, errors
- it is possible to rewind to a snapshot, and retry whatever you were doing safely without any side effects

#### cons:
- expensive license
- huge cost for resources
- huge overhead (memory and CPU)
- huge image size
- automation of creating images is close to zero. Images cannot be created by CI
- incompatible with Git (you cannot git commit the images/snapshots)
- development in a VM is an overkill (I have never heard about anyone doing it...)
- very slow deployment
- every time you set up the system it takes much effort and time, and it is very manual, repetetive, slow

https://jaxenter.com/nobody-puts-java-container-139373.html

# Microservices

### Reading:
* #### Microservices in Java
  [Microservices patterns to support building systems that are tolerant of failure.](https://dzone.com/refcardz/learn-microservices-in-java)

* #### Getting started with Microservices
  [Design Patterns for Decomposing the Monolith](https://dzone.com/refcardz/getting-started-with-microservices)

* #### Microservices - a definition of this new architectural term
  [https://martinfowler.com/articles/microservices.html](https://martinfowler.com/articles/microservices.html)
[https://www.sigs-datacom.de/uploads/tx_dmjournals/fowler_lewis_OTS_Architekturen_15.pdf](https://www.sigs-datacom.de/uploads/tx_dmjournals/fowler_lewis_OTS_Architekturen_15.pdf)

* #### Twelve‑Factor App for Microservices
  [https://www.nginx.com/blog/microservices-reference-architecture-nginx-twelve-factor-app/](https://www.nginx.com/blog/microservices-reference-architecture-nginx-twelve-factor-app/)

### Topics for discussion:
* separation between development environment and runtime environment,
* dockers cannot be used to develop,
* docker images should be only used to run as a reliable bundle/packages,
* developers must work "on bare metal",
* if you have source code you can work on it any way you want, especially hot swap and debugger. Docker prevents your from flexibility,
* present how Spring Initializr works https://start.spring.io,
* create a configurator wizard to generate a bundle with application.properties for each microservice

### The Requirements:
* after issuing a command git clone git@microservice && gradle bootRun for just one microservice, everything must work with default settings, out-of-the-box,
* the docker must be done by CI/CD Jenkins only, etc.
* the project source code should have minimal information about the packaging format and packaging destination,
* the project source cannot contain any information where it's deployed or it's configuration.
* you are not allowed to run other commands to start the project,
* you are not allowed to access disk to start the project,
* you are not allowed to create symbolic links to start the project,
* you must be able to work with your own database, and without it (both options should be available), the default must be always embedded, pre-initialized with JPA API, which means you are not allowed to use native SQL scripts,
* you must be able to run it with the debugger,
* you are not allowed to run it with Ant or Shell commands, it has to run in IntelliJ directly,
* must run on any OS. You are not allowed to run it on Linux only, it has to work on Windows, and all other platforms,
* you are not allowed to run it on Java 5, 6, 7, etc... It has to compile to the current stable Java release, or newer i.e. Java 17

THE MOST IMPORTANT RULE:
* for development purposes, you are not allowed to run it on a machine where DOCKER is available. For example: RedHat 7 Linux doesn't have docker installed. (the only assumption you can  make is that it is the machine inside VPN network, and all dependent microservices are up and running somewhere there, and they have static addresses and ports)
* the limit to set up and run the entire infrastructure must be 5 minutes (maximum two shell commands/lines).
* the property file should be empty with all configuration commented out, and be available for all developers to modify any line at anytime.

### How to achieve this:

Microservice and Docker are unrelated concepts and they must be separated. The entire infrastructure must run without dockers first! You are free to add dockers after the project is completed. One should be able to be replace a docker technology at any moment, with any other technology. Using Dockers for development is bad. It is as bad as using VMs. No one sane uses Virtual Machine to develop software. Virtualization and Dockers are used for distribution of sotware in a standard format. Creating a docker image every commit is also not good. The docker images are heavy and should be distributed only at the end of the development/release cycle. Dockers are very nice, if you don't know anything about software and you don't want to install java, database, Maven, Gradle, Tomcat, ElasticSearch, etc... If you are a programmer, and you use dockers, you are missing the point. Docker is very convinient way of running software, which doesn't belong to you and you don't have access to the source code. If you have access to the source code 9/10 times you should prefer running it natively from the source code. Especially when the project was written in Java programming language where compilation is JIT.

### Conclusions: (rethink...)
There should always be a default configuration provided, just to start with some running sample. Configuration is a repetitive step that should be automated and done only once. Developers should rarely configure environments. The Development environment should be different than the runtime environment. Maven/Gradle are tools to fully set up your IDE (IntelliJ, Eclipse), classpath, dependencies, jdk, etc. for you to work as soon as possible. You do not have to change properties of IntelliJ to start coding. Maven/Gradle have profiles. There should always be profiles for developers, for testers and for different customers. DevOps should modify these configuration for the runtime environments and should adjust them for the cluster/server environment. Everything that is used by Operation/Customer should not be touched by developers. Everything that is used by Developers should not be touched by Operation/Customer. DevOps may provide a sample configuration for both scenarios.

Developers can learn to set up everything. but this is not optimal. Developers are expensive and making them setup the infrastructure or resolve network issue is not good. Yes sure, we can all do it. Just because we can write any code and because we can learn to set up any piece of software that doesn't mean we should do it. This is waste of resources. A developer doesn't have to be aware of how the Dockers and Kubernetes work. It is nice to know it, but it is not necessary to create and compile Java classes.

### Versioning
Working with a microservice requires running all the dependent microservices. It is best if they are running on a remote server inside the network. In ideal scenario every user gets ability to connect to any version of any dependency. For example microservice A ver.1.0.0 and the dependent microservice B ver.2.0.0. This requires all microservices to run. In order to save money, we can only run major releases. If the developer needs dependency ver.1.5.0 (major 1; minor 5 version) he needs to deploy this microservice on the cloud and terminate it after he is done with his work. If the developer needs a specific build e.g. ver.1.6.98 he should run it on his local environment. This is to be discussed. There are two ways to run it: a) docker image, b) on bare metal. This is also to be discussed. I would prefer to leave a choice to the user. Anyone should be able to choose what and how one wants to run the dependencies on his local environment.
If the default scenario is not sufficient because the dependent microservice instance is overloaded, or because the instance cannot be shared, two developers must have separate instances of the same major version of the microservice, there should be an option to run the same major version of microservice at different/dedicated host:port on the cloud.
If running a dedicated instance of the microservice on the cloud is not an option, then the developer must run the microservices on a local machine.
In the worst case scenario, the developer must run all microservices on his local host.
With all the above solutions another problem arises: which version of microservice dependency is needed? What if two microservices (e.g. A, B) need a microservice C. Microservice A needs C ver.1.0.0, and microservice B needs also C (but this time ver.2.0.0). It means that we may have to run multiple instances of microservice C (ver.1.0.0 and ver.2.0.0) for each user. In the worst case scenario: there is 10 microservices, there is 20 developers, there is 100 versions. 10x20x100=20000 instances. The most common, and at the same time the most primitive solution is to not worry about the versions of the dependencies and use the most recent Snapshots (latest) that are running and each instance is shared accross all teams. This solves many issues, but requires unbelievable amount of discipline and communication between the developers. I really doubt this will work with more than three microservices... The proper solution is to keep track of versions and maintaina strict graph of dependencies / manifest in git, just like pom.xml does for binaries/jars. It is extra work for the developers but it is an industry standard and the project becomes very reliable, normalized and compact. No redundant microservices will be shipped to the customer during the release, only the minimal set of runtime dependencies declared in the IaC repository. This is the cleanest design that you can get.

### Dependencies topology
https://en.wikipedia.org/wiki/Network_topology#/media/File:NetworkTopologies.svg
* Fully connected graph (the worst idea for maintainance)
* Line/Chained (look at ISO/OSI model) cons: single point of failure pro: the cleanest design you can imagine
* Star (good but very limited) cons: central authority like in Communism
* Star of Stars (optimal)
* Tree
* isolated clusters of nested isolated clusters
* cycles in the graph cause inability to switch just one node to localhost

How to address problem of teams working remotely, outside of our premises. Can anyone simply checkout one microservice?
Maybe the solution is to put everything into one large git repository, just like the monolith?

### Kafka

### Rollbacks
Kafka is something that has to be considered. Ideally all microservices should be communicated by Kafka, just in case there is a need to reset and resend the event.

### Kubernetes

### Minikube

### Load balancers

# Git

### Ignore OS specific files
Felt tip: since you probably never want to include .DS_Store files, make a global rule. First, make a global .gitignore file somewhere, e.g. `echo .DS_Store >> ~/.gitignore_global`. Now tell git to use it for all repositories: `git config --global core.excludesfile ~/.gitignore_global`.

### Branching workflows
https://backlog.com/git-tutorial/branching-workflows/

### example script that creates a sequence of versions
This is not the best example because the branches should be created of master, not the previous branch! So this is something simple that will result in a long chain of nested branches. All this can be avoided with changing the worklflow. First checkout master and then create a new branch. Always from master.

```bash
#!/bin/zsh

current_dir=$PWD
cd ~/Workspace/software-development/

today=$(date "+%Y-%m-%d")
branch=$(git symbolic-ref -q --short HEAD)

if [ "$today" != "$branch" ]
then
    git checkout -b $today
fi
git add .
git commit

# instead of: git checkout master && git merge $today
git fetch . ${today}:master

#instead of: git checkout master && git pull master
git pull origin master:master

# puch all branches ($today, and master) to different upstream urls with just one command
git push --all
cd $current_dir
```

### Undo a commit, making it a topic branch
Very often I experiment on something I make 100 commits and then I realize: "I am working on the wrong branch". What to do?

Solution 1:
```git
$ git branch topic/wip/correct-branch-name      (1)
$ git reset --hard HEAD~3                       (2)
$ git checkout topic/wipcorrect-branch-name     (3)
```
You have made some commits, but realize they were premature to be in the master branch. You want to continue polishing them in a topic branch, so create topic/wip branch off of the current HEAD.

Rewind the master branch to get rid of those three commits.

Switch to topic/wip branch and keep working.

### How to fix a commit
When I was working on a feature, after 3 days of coding I found out that my code is not working. I decided to check if it was working before. `git checkout master` works `git checkout my-feature` does not work (the most recent commit - HEAD). I performed a binary search between commits. I found which one was wrong. I found the bug. I don't want to loose any work, I want to keep everything, and add this change into the middle of my commit history. How to do it?

I did: `git checkout broken-commit`, fixed everything, then: `git commit --fixup 7e866455` and everything worked from now on.
at the end you need to remember to execute:
```
$ git rebase -i --autosquash 0e2977b5
```
as described below:
https://dev.to/smichaelsen/fixing-git-commits---you-always-did-it-wrong-4oi2

## Spring
### application.properties
In order to make the project configurable, all Strings should be put as constants or variables. When the value may change in the future, we may want to choose another value without recompiling the project. In such scenario add `@Value` annotation over `private String someProperty;` for example:
```java  
@Value("${some.property:messages}")
pivate String someProperty;
```
in this case, no further action is needed because `private String resourceName` is initialized with default value: `"messages"`.
If you want to overwrite the default value, simply put the line `property.name = messages` in `application.properties` file.

## Spring Boot

### Using QueryDSL annotation processor with Gradle and IntelliJ IDEA
https://stackoverflow.com/questions/49540962/mapstruct-annotation-processor-does-not-seem-to-work-in-intellij-with-gradle-pro
In Settings -> Build, Execute, Deployment -> Build Tools -> Gradle -> Runner -> Delegate IDE build/run actions to Gradle
Run tests using:
Gradle Test Runner:

### 90. Hot Swapping
https://docs.spring.io/spring-boot/docs/current/reference/html/howto-hotswapping.html

### 91. Build
https://docs.spring.io/spring-boot/docs/current/reference/html/howto-build.html

# Software Architecture
### 5 reasons why you should use field-based access with JPA and Hibernate
http://mvnt.us/m920256

### REST vs GraphQL
https://blog.goodapi.co/rest-vs-graphql-a-critical-review-5f77392658e7

### Building a Gateway Using Spring MVC
https://cloud.spring.io/spring-cloud-gateway/1.0.x/multi/multi__building_a_gateway_using_spring_mvc.html

### Linux shell
[Explain Shell](https://explainshell.com/explain?cmd=usermod+-a+-G+sudo+username)

# read this:
The Initiative for Open Authentication (OATH)

https://openauthentication.org

https://www.varonis.com/blog/what-is-oauth/

https://www.cubrid.org/blog/dancing-with-oauth-understanding-how-authorization-works


https://jwt.io/

# my todo list:
* create styles with html as here: https://developer.github.com/v3/

